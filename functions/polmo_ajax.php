<?php
/**
 * Created by PhpStorm.
 * User: Khalimov Renato
 * Date: 23.04.2017
 * Time: 13:29
 */

add_action('wp_ajax_nopriv_showMeMoreStupidMachine', 'showMeMoreStupidMachine');
add_action('wp_ajax_showMeMoreStupidMachine', 'showMeMoreStupidMachine');

function showMeMoreStupidMachine()
{
    global $wpdb;
    $params = array();
    parse_str(esc_sql($_POST['data']), $params);
    $data = $wpdb->get_results("
            SELECT *,
                (select group_concat(carcase)
                from `" . $wpdb->prefix . "polmo_app`
                where id = s.id) as 'carcase',
                (select group_concat(year_from)
                from `" . $wpdb->prefix . "polmo_app`
                where id = s.id) as 'year_from',
                (select group_concat(year_to)
                from `" . $wpdb->prefix . "polmo_app`
                where id = s.id) as 'year_to',
                (select group_concat(hp)
                from `" . $wpdb->prefix . "polmo_app`
                where id = s.id) as 'hp'
            FROM `" . $wpdb->prefix . "polmo_price` p
            INNER JOIN `" . $wpdb->prefix . "polmo_s` s
            ON p.polmo_code = s.polmo_code
            WHERE s.id IN 
                (select `id` 
                from `" . $wpdb->prefix . "polmo_app`s
                where `manufacturer` = '" . $params['manufacturer'] . "'
                and `model` = '" . $params['model'] . "'
                and `version` = '" . $params['version'] . "')
            AND p.price > 0
            ORDER BY `id`, `carcase`, `hp` ASC", ARRAY_A);
    for ($i = 0; $i < count($data); ++$i)
        $data[$i]['price'] = makePrice($data[$i]['price'], 'polmostrow');
    echo json_encode($data, JSON_UNESCAPED_UNICODE);
    die();
}

add_action('wp_ajax_nopriv_addToCart', 'addToCart');
add_action('wp_ajax_addToCart', 'addToCart');
/**
 * Добавляем товар в корзину
 */
function addToCart()
{
    $data = array();
    if (!isset($_SESSION['cart']))
        $_SESSION['cart'] = array();
    $data[esc_sql($_POST['data']['type']) . '-' . esc_sql($_POST['data']['id'])] = 1;
    if (isset($_SESSION['cart'][key($data)]))
        ++$_SESSION['cart'][key($data)];
    else
        $_SESSION['cart'][key($data)] = 1;
    echo count($_SESSION['cart']);
    die();
}

add_action('wp_ajax_nopriv_delFromCart', 'delFromCart');
add_action('wp_ajax_delFromCart', 'delFromCart');
/**
 * Удаление товара из корзины
 */
function delFromCart()
{
    unset($_SESSION['cart'][esc_sql($_POST['data'])]);
    die();
}

add_action('wp_ajax_nopriv_clearCart', 'clearCart');
add_action('wp_ajax_clearCart', 'clearCart');
/**
 * Очистка корзины
 */
function clearCart()
{
    unset($_SESSION['cart']);
    die();
}

/**
 * Новая почта
 */
add_action('wp_ajax_nopriv_showDataNP', 'showDataNP');
add_action('wp_ajax_showDataNP', 'showDataNP');

function showDataNP()
{
    $data = array();
    $np = new \LisDev\Delivery\NovaPoshtaApi2(get_option('np_key'));
    switch ($_POST['type']) {
        case 'np_area':
            $cities = $np->getCitiesByRegion(esc_sql($_POST['ref']));
            $i = -1;
            foreach ($cities as $city){
                ++$i;
                $data['np'][$i]['description'] = $city['DescriptionRu'];
                $data['np'][$i]['ref'] = $city['Ref'];
            }
            $data['type'] = 'np_city';
            break;
        case 'np_city':
            $cities = $np->getWarehouses(esc_sql($_POST['ref']));
            $i = -1;
            foreach ($cities['data'] as $city){
                ++$i;
                $data['np'][$i]['description'] = $city['DescriptionRu'];
                $data['np'][$i]['ref'] = $city['Ref'];
            }
            $data['type'] = 'np_warehouse';
            break;
    }
    echo json_encode($data, JSON_UNESCAPED_UNICODE);
    die();
}
