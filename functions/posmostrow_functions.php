<?php
/**
 * Created by PhpStorm.
 * User: Khalimov Renato
 * Date: 23.04.2017
 * Time: 10:24
 */

/**
 * @return array
 * все производители, модели и спецификации на главной
 */
function showManufacturers()
{
    global $wpdb;
    if (is_front_page()) {
        $manufacturers = $wpdb->get_results("
                        SELECT `id`, `manufacturer`, `model`, `version` 
                        FROM `" . $wpdb->prefix . "polmo_app` 
                        GROUP BY `manufacturer`,`model`, `version` 
                        ORDER BY `manufacturer` ASC", ARRAY_A);
        $data = array();
        foreach ($manufacturers as $manufacturer)
            if ($manufacturer['manufacturer'])
                $data[$manufacturer['manufacturer']][$manufacturer['model']][$manufacturer['id']] = $manufacturer['version'];

    }

    return $data;
}

/**
 * Вывод глушителей
 *
 */
function showData($table)
{
    global $wpdb;
    $universals = $wpdb->get_results("
              SELECT *
              FROM `" . $wpdb->prefix . "gl_" . $table . "`  
              ORDER BY `id` ASC", ARRAY_A);

    for ($i = 0; $i < count($universals); ++$i) {
        $universals[$i]['price'] = makePrice($universals[$i]['price'], $table);
        $universals[$i]['img'] = get_template_directory_uri() . '/' . $universals[$i]['img'];
    }
    return json_encode($universals, JSON_UNESCAPED_UNICODE);
}

/**
 * @param $price
 * @return float
 * формируем цену
 */
function makePrice($price, $table)
{
    return round(((float)$price + ((float)$price * (get_option('percent_' . $table)) / 100)) * (float)get_option('currency'));
}

/**
 * @param $phone
 * @return mixed
 */
function sanitazePhone($phone)
{
    return str_replace(array('(', ')', '-', ' '), '', $phone);
}

/**
 * @return array
 */
function showCartItems()
{
    if (isset($_SESSION['cart'])) {
        global $wpdb;
        $items = array();
        $results = array();
        foreach ($_SESSION['cart'] as $key => $value) {
            $key = explode('-', $key);
            $items[$key[0]][$key[1]] = $value;
        }
        foreach ($items as $key => $value) {
            $ids = implode(',', array_keys($value));
            if ($key != 'polmostrow') {
                $data = $wpdb->get_results("
                            SELECT `id`, `name`, `price`, `img`
                            FROM `" . $wpdb->prefix . "gl_" . $key . "`  
                            WHERE `id` IN(" . $ids . ")
                            ORDER BY `id` ASC", ARRAY_A);
            } else {
                $data = $wpdb->get_results("
                            SELECT `polmo_code` as id, polmo_code, `description` as name, `price`
                            FROM `" . $wpdb->prefix . "polmo_price`  
                            WHERE `polmo_code` IN(" . $ids . ")
                            ORDER BY `polmo_code` ASC", ARRAY_A);
            }
            foreach ($data as $item) {
                if(isset($item['polmo_code']))
                    $item['name'] = $item['polmo_code'] . ' - ' . $item['name'];
                $item['count'] = $value[$item['id']];
                $item['price'] = makePrice($item['price'], $key);
                $item['total_price'] = $item['price'] * $item['count'];
                $item['img'] = get_template_directory_uri() . '/' . $item['img'];
                $item['session_key'] = $key . '-' . $item['id'];
                array_push($results, $item);
            }
        }
    }
    return $results;
}

function searchResult()
{
    if (isset($_POST['search'])) {
        global $wpdb;
        if (count($_POST['type']) > 0 && $_POST['type'] != 'all' && !empty($_POST['type'])) {
            $data = $wpdb->get_results("
                    SELECT *
                    FROM `" . $wpdb->prefix . 'gl_' . esc_sql($_POST['type']) . "` 
                    WHERE `name` LIKE '%" . esc_sql(trim($_POST['search'])) . "%'
                    ORDER BY `id` ASC", ARRAY_A);
            for ($i = 0; $i < count($data); ++$i) {
                $data[$i]['price'] = makePrice($data[$i]['price'], $_POST['type']);
                $data[$i]['img'] = get_template_directory_uri() . '/' . $data[$i]['img'];
            }
            return json_encode($data, JSON_UNESCAPED_UNICODE);
        } else {
            $tables = array(
                'attachments',
                'elems',
                'gofra',
                'sports',
                'stronger',
                'universal',
                'tube');
            foreach ($tables as $table) {
                $data = $wpdb->get_results("
                    SELECT *
                    FROM `" . $wpdb->prefix . 'gl_' . $table . "` 
                    WHERE `name` LIKE '%" . esc_sql(trim($_POST['search'])) . "%'
                    ORDER BY `id` ASC", ARRAY_A);
                if ($data) {
                    for ($i = 0; $i < count($data); ++$i) {
                        $data[$i]['price'] = makePrice($data[$i]['price'], $table);
                        $data[$i]['img'] = get_template_directory_uri() . '/' . $data[$i]['img'];
                    }
                    return json_encode($data, JSON_UNESCAPED_UNICODE);
                }
            }
//            $data = $wpdb->get_results("
//                    (SELECT `id`, `name`, `img`, `price`, `column`
//                    FROM `" . $wpdb->prefix . "gl_attachments`
//                    WHERE `name` LIKE '%" . esc_sql(trim($_POST['search'])) . "%'
//                    ORDER BY `id` ASC)
//                    UNION
//                    (SELECT `id`, `name`, `img`, `price`, `column`
//                    FROM `" . $wpdb->prefix . "gl_elems`
//                    WHERE `name` LIKE '%" . esc_sql(trim($_POST['search'])) . "%'
//                    ORDER BY `id` ASC)
//                    UNION
//                    (SELECT `id`, `name`, `img`, `price`, `column`
//                    FROM `" . $wpdb->prefix . "gl_gofra`
//                    WHERE `name` LIKE '%" . esc_sql(trim($_POST['search'])) . "%'
//                    ORDER BY `id` ASC)
//                    UNION
//                    (SELECT `id`, `name`, `img`, `price`, `column`
//                    FROM `" . $wpdb->prefix . "gl_sports`
//                    WHERE `name` LIKE '%" . esc_sql(trim($_POST['search'])) . "%'
//                    ORDER BY `id` ASC)
//                    UNION
//                    (SELECT `id`, `name`, `img`, `price`, `column`
//                    FROM `" . $wpdb->prefix . "gl_stronger`
//                    WHERE `name` LIKE '%" . esc_sql(trim($_POST['search'])) . "%'
//                    ORDER BY `id` ASC)
//                    UNION
//                    (SELECT `id`, `name`, `img`, `price`, `column`
//                    FROM `" . $wpdb->prefix . "gl_universal`
//                    WHERE `name` LIKE '%" . esc_sql(trim($_POST['search'])) . "%'
//                    ORDER BY `id` ASC)
//                    UNION
//                    (SELECT `id`, `name`, `img`, `price`, `column`
//                    FROM `" . $wpdb->prefix . "gl_tube`
//                    WHERE `name` LIKE '%" . esc_sql(trim($_POST['search'])) . "%'
//                    ORDER BY `id` ASC)
//                    ", ARRAY_A);
        }

    }
}