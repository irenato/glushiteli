<?php
/**
 * Created by PhpStorm.
 * User: renato
 * Date: 06.05.17
 * Time: 21:05
 */

/**
 * Template name: Cart
 */
if (!isset($_SESSION['cart'])) {
    wp_redirect(get_home_url(), '301');
    exit();
}

get_header();
?>

    <!--START CONTENT-->
    <main>
        <?php if (have_posts()) : while (have_posts()) :
        the_post(); ?>
        <div class="cart">
            <div class="baner_cart">
                <div class="container">
                    <div class="row">
                        <h3><?php the_title(); ?></h3>
                        <?php get_template_part('template-parts/search_form') ?>
                        <p class="hint"><?= strip_tags(get_the_content()); ?></p>

                    </div>
                </div>
            </div>
            <?php endwhile; ?>
            <?php endif; ?>
            <div class="tab_nav">
                <div class="container">
                    <div class="row">
                        <?php get_template_part('template-parts/main_menu') ?>
                    </div>
                </div>
            </div>
            <div class="main">
                <div class="container">
                    <div class="row">
                       <p id="hiddenData" style="display: none;" data-view="6"></p>
                        <div class="mess" style="display: none;">Ваша корзина пуста. Воспользуйтесь быстрой навигацией
                            или поиском для добавления товаров в корзину.
                        </div>

                        <div class="buttons">
                            <div class="button">
                                <a href="#delGoodsInCart">
                                    Очистить корзину
                                </a>
                            </div>
                        </div>

                        <div class="cart_wrapp">
                            <ul class="cart_header">
                                <li>
                                    <span class="img-holder">ТОВАР</span>
                                    <span class="title">НАЗВАНИЕ</span>
                                    <span class="price">ЦЕНА ЗА 1 ЕД.</span>
                                    <span class="quantity">КОЛ-ВО</span>
                                    <span class="total_price">ИТОГО:</span>
                                    <span class="del"></span>
                                </li>
                            </ul>
                            <?php $data = showCartItems() ?>
                            <ul class="cart_body">
                                <?php if ($data): ?>
                                <?php foreach ($data as $item): ?>
                                    <li>
                                    <span class="img-holder">
				            		<img src="<?= $item['img'] ?>" alt="<?= $item['name'] ?>">
				            	</span>
                                        <span class="title"> <?= $item['name'] ?></span>
                                        <span class="price"><?= $item['price'] ?> грн.</span>
                                        <span class="quantity">
				            		<i class="fa fa-minus"></i>
									<input type="text" value="<?= $item['count'] ?>">
									<i class="fa fa-plus"></i>

				            	</span>
                                        <span class="total_price"><?= $item['total_price'] ?> грн.</span>
                                        <span class="del" data-key="<?= $item['session_key'] ?>">
	                                <i class="fa fa-close"></i>
	                                <i>Удалить</i>
                                </span>
                                    </li>
                                <?php endforeach; ?>
                            </ul>
                            <ul class="cart_result">
                                <li>
                                    <span>Общее количество:</span>
                                    <span class="r_quantity quantity">2</span>
                                    <span class="r_total_price total_price">1258.54 грн.</span>
                                    <span></span>
                                </li>
                            </ul>
                            <div class="buttons">
                                <div class="button orange hov">
                                    <a href="#order_form">
                                        Заказать
                                    </a>
                                </div>
                            </div>
                        <?php endif; ?>
                        </div>
                        <div id="order_form">
                            <form action="#" method="post" id="modal_form">
                                <div class="step step1 current" data-index="1" style="display: block;">
                                    <p class="body-title">Поля помеченные <span>*</span> обязательны для заполнения</p>
                                    <div class="form-group">
                                        <input type="text" class="form-control" name="firstName" data-valid="firstName"
                                               placeholder="Ваше имя и фамилия *">
                                        <p class="error">Введите имя и фамилию</p>
                                    </div>
                                    <div class="form-group">
                                        <input type="text" class="form-control" name="email" 
                                               placeholder="Ваш e-mail ">
                                        <p class="error">Проверьте правильность введенного эл.адреса</p>
                                    </div>
                                    <div class="form-group">
                                        <input type="text" class="form-control" name="phone" data-valid="phone"
                                               placeholder="Ваш номер телефона *">
                                        <p class="error">Проверьте правильность введенного номера телефона</p>
                                    </div>
                                    <div class="form-group">
                                        <input type="text" class="form-control" name="city" placeholder="Ваш город">
                                    </div>
                                    <div class="form-group">
                                        <input type="text" class="form-control" name="year"
                                               placeholder="Год выпуска автомобиля">
                                    </div>
                                    <div class="form-group">
                                        <input type="text" class="form-control" name="piston"
                                               placeholder="Объем двигателя автомобиля">
                                    </div>
                                    <div class="form-group">
                                        <input type="text" class="form-control" name="body"
                                               placeholder="Кузов (седан, хетчбек, универсал)">
                                    </div>
                                </div>
                                <div class="step step2" data-index="2">
                                    <p class="body-title">Выберите, пожалуйста способ доставки товара</p>
                                    <div class="radio">
                                        <input type="radio" name="optradio" hidden id="r1">
                                        <label for="r1"><b>Не могу выбрать перевозчика.</b></label>
                                    </div>
                                    <div class="radio">
                                        <input type="radio" name="optradio" hidden id="r2">
                                        <label for="r2"><b>Интайм.</b> Адреса представительств в Вашем городе можно
                                            посмотреть на сайте: <a href="http://www.intime.ua" target="_blank"
                                                                    class="link">www.intime.ua.</a> Доставку товара
                                            оплачивает покупатель.</label>
                                        <div class="select">
                                            <p>Выберите свой город и введите номер отделения</p>
                                            <select name="intime_area" class="form-control np_area">
                                                <?php $areas = $np->getAreas(); ?>
                                                <?php if ($areas['success']): ?>
                                                    <?php $i = -1; ?>
                                                    <?php foreach ($areas['data'] as $area): ?>
                                                        <?php if (++$i > 0): ?>
                                                            <?php $area_data = $np->getArea($area['Description']); ?>
                                                            <option
                                                                value="<?= $area['Ref'] ?>"><?= $area_data['data'][0]['AreaRu'] ?></option>
                                                        <?php endif; ?>
                                                    <?php endforeach; ?>
                                                <?php endif; ?>
                                            </select>
                                            <?php $cities = $np->getCitiesByRegion($areas['data'][1]['Ref']); ?>
                                            <select name="intime_city" class="form-control np_city">
                                                <option value="#" disabled>Выберите город</option>
                                                <?php if (is_array($cities)): ?>
                                                    <?php foreach ($cities as $area): ?>
                                                            <option
                                                                value="<?= $area['Ref'] ?>"><?= $area['DescriptionRu'] ?></option>
                                                    <?php endforeach; ?>
                                                <?php endif; ?>
                                            </select>
                                            <input type="text" class="form-control" placeholder="Номер отделения">
                                        </div>
                                    </div>
                                    <div class="radio">
                                        <input type="radio" name="optradio" hidden id="r3">
                                        <label for="r3"><b>Новая Почта.</b> Адреса представительств в Вашем городе можно
                                            посмотреть на сайте: <a href="http://www.novaposhta.ua." target="_blank"
                                                                    class="link">www.novaposhta.ua.</a> Доставку товара
                                            оплачивает покупатель.</label>
                                        <div class="select">
                                            <p>Выберите свой город и номер отделения</p>
                                            <select name="np_area" class="form-control np_area">
                                                <?php $areas = $np->getAreas(); ?>
                                                <?php if ($areas['success']): ?>
                                                    <?php $i = -1; ?>
                                                    <?php foreach ($areas['data'] as $area): ?>
                                                        <?php if (++$i > 0): ?>
                                                            <?php $area_data = $np->getArea($area['Description']); ?>
                                                            <option
                                                                value="<?= $area['Ref'] ?>"><?= $area_data['data'][0]['AreaRu'] ?></option>
                                                        <?php endif; ?>
                                                    <?php endforeach; ?>
                                                <?php endif; ?>
                                            </select>
                                            <!-- </select> -->
                                            <?php $cities = $np->getCitiesByRegion($areas['data'][1]['Ref']); ?>
                                            <select name="np_city" class="form-control np_city">
                                                <option value="#" disabled>Выберите город</option>
                                                <?php if (is_array($cities)): ?>
                                                    <?php foreach ($cities as $area): ?>
                                                            <option
                                                                value="<?= $area['Ref'] ?>"><?= $area['DescriptionRu'] ?></option>
                                                    <?php endforeach; ?>
                                                <?php endif; ?>
                                            </select>
                                            <?php $warehouses = $np->getWarehouses($cities[0]['Ref']); ?>
                                            <select name="np_warehouse" class="form-control np_warehouse">
                                                <option value="#" disabled>Выберите отделение</option>
                                                <?php if ($warehouses['success']): ?>
                                                    <?php foreach ($warehouses['data'] as $area): ?>
                                                        <option
                                                            value="<?= $area['Ref'] ?>"><?= $area['DescriptionRu'] ?></option>
                                                    <?php endforeach; ?>
                                                <?php endif; ?>
                                            </select>
                                        </div>
                                    </div>


                                    <div class="radio">
                                        <input type="radio" name="optradio" hidden id="r4">
                                        <label for="r4"><b>Деливери.</b> Адреса представительств в Вашем городе можно
                                            посмотреть на сайте: <a href="http://www.delivery-auto.com.ua/"
                                                                    target="_blank" class="link">www.delivery-auto.com.ua.</a>
                                            Доставку товара оплачивает покупатель.</label>


                                        <div class="select">
                                            <p>Выберите свой город и введите номер отделения</p>
                                            <select name="delivery_area" class="form-control np_area">
                                                <?php $areas = $np->getAreas(); ?>
                                                <?php if ($areas['success']): ?>
                                                    <?php $i = -1; ?>
                                                    <?php foreach ($areas['data'] as $area): ?>
                                                        <?php if (++$i > 0): ?>
                                                            <?php $area_data = $np->getArea($area['Description']); ?>
                                                            <option
                                                                value="<?= $area['Ref'] ?>"><?= $area_data['data'][0]['AreaRu'] ?></option>
                                                        <?php endif; ?>
                                                    <?php endforeach; ?>
                                                <?php endif; ?>
                                            </select>
                                            <?php $cities = $np->getCitiesByRegion($areas['data'][1]['Ref']); ?>
                                            <select name="delivery_city" class="form-control np_city">
                                                <option value="#" disabled>Выберите город</option>
                                                <?php if (is_array($cities)): ?>
                                                    <?php foreach ($cities as $area): ?>
                                                            <option
                                                                value="<?= $area['Ref'] ?>"><?= $area['DescriptionRu'] ?></option>
                                                    <?php endforeach; ?>
                                                <?php endif; ?>
                                            </select>
                                            <input type="text" class="form-control" placeholder="Номер отделения">
                                        </div>
                                    </div>


                                    <div class="radio">
                                        <input type="radio" name="optradio" hidden id="r5">
                                        <label for="r5"><b>САТ.</b> Адреса представительств в Вашем городе можно
                                            посмотреть на сайте: <a href="https://www.sat.ua/" target="_blank"
                                                                    class="link">www.sat.ua.</a> Доставку товара
                                            оплачивает покупатель.</label>
                                        <div class="select">
                                            <p>Выберите свой город и введите номер отделения</p>
                                            <select name="sat_area" class="form-control np_area">
                                                <?php $areas = $np->getAreas(); ?>
                                                <?php if ($areas['success']): ?>
                                                    <?php $i = -1; ?>
                                                    <?php foreach ($areas['data'] as $area): ?>
                                                        <?php if (++$i > 0): ?>
                                                            <?php $area_data = $np->getArea($area['Description']); ?>
                                                            <option
                                                                value="<?= $area['Ref'] ?>"><?= $area_data['data'][0]['AreaRu'] ?></option>
                                                        <?php endif; ?>
                                                    <?php endforeach; ?>
                                                <?php endif; ?>
                                            </select>
                                            <?php $cities = $np->getCitiesByRegion($areas['data'][1]['Ref']); ?>
                                            <select name="sat_city" class="form-control np_city">
                                                <option value="#" disabled>Выберите город</option>
                                                <?php if (is_array($cities)): ?>
                                                    <?php foreach ($cities as $area): ?>
                                                            <option
                                                                value="<?= $area['Ref'] ?>"><?= $area['DescriptionRu'] ?></option>
                                                    <?php endforeach; ?>
                                                <?php endif; ?>
                                            </select>
                                            <input type="text" class="form-control" placeholder="Номер отделения">
                                        </div>
                                    </div>
                                    <div class="radio">
                                        <input type="radio" name="optradio" hidden id="r6">
                                        <label for="r6"><b>Самовывоз.</b> Товар можно забрать в нашем офисе.</label>
                                    </div>
                                </div>
                                <div class="step step3" data-index="3">
                                    <p class="body-title">Выберите, пожалуйста способ оплаты</p>
                                    <div class="radio">
                                        <input type="radio" name="optradio1" hidden id="ro1">
                                        <label for="ro1">Перевод на карту ПриватБанка</label>
                                    </div>
                                    <div class="radio">
                                        <input type="radio" name="optradio1" hidden id="ro2">
                                        <label for="ro2">Наложенным платежом</label>
                                    </div>

                                    <div class="form-group">
                                        <textarea class="form-control" placeholder="Сообщение..."></textarea>
                                    </div>
                                </div>
                                <div class="buttons">
                                    <div class="button grey" data-action="prev"><a href="#">Назад</a></div>
                                    <input type="submit" class="button orange" value="Отправить">
                                    <div class="button green" data-action="next"><a href="#">Далее</a></div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </main>

<?php
get_footer();

