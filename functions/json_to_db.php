<?php
/**
 * Created by PhpStorm.
 * User: renato
 * Date: 01.05.17
 * Time: 10:57
 */

//add_action('admin_init', 'fill_gl_universal_table');

function fill_gl_universal_table()
{
    global $wpdb;
    $wpdb->query('TRUNCATE ' . $wpdb->prefix . 'gl_universal');
    $data = json_decode(file_get_contents(get_template_directory_uri() . "/json/universal.json"), true);
    foreach ($data as $item) {
        $fields = array(
            'name' => $item['name'],
            'producer' => $item['producer'],
            'full_name' => $item['full_name'],
            'img' => 'images/catalog/universal/' . explode('/', $item['name'])[0] . '.jpg',
            'long' => (int)$item['long'],
            'width' => (int)$item['shir'],
            'd_enter' => (int)$item['d_enter'],
            'd_ex' => (int)$item['d_ex'],
            'price' => (float)$item['price']
        );
        $wpdb->insert($wpdb->prefix . 'gl_universal', $fields, '');
    }

}

//add_action('admin_init', 'fill_gl_sports_table');

function fill_gl_sports_table()
{
    global $wpdb;
    $wpdb->query('TRUNCATE ' . $wpdb->prefix . 'gl_sports');
    $data = json_decode(file_get_contents(get_template_directory_uri() . "/json/sport.json"), true);
    foreach ($data as $item) {
        $fields = array(
            'code' => $item['kod'],
            'name' => $item['name'],
            'producer' => $item['producer'],
            'long' => (float)$item['long'],
            'diam' => (float)$item['diam'],
            'img' => $item['img'],
            'price' => (float)$item['price']
        );
        $wpdb->insert($wpdb->prefix . 'gl_sports', $fields, '');
    }

}

//add_action('admin_init', 'fill_gl_tube_table');

function fill_gl_tube_table()
{
    global $wpdb;
    $wpdb->query('TRUNCATE ' . $wpdb->prefix . 'gl_tube');
    $data = json_decode(file_get_contents(get_template_directory_uri() . "/json/truba.json"), true);
    foreach ($data as $item) {
        $fields = array(
            'name' => $item['name'],
            'price' => (float)$item['price']
        );
        $wpdb->insert($wpdb->prefix . 'gl_tube', $fields, '');
    }

}

//add_action('admin_init', 'fill_gl_stronger_table');

function fill_gl_stronger_table()
{
    global $wpdb;
    $wpdb->query('TRUNCATE ' . $wpdb->prefix . 'gl_stronger');
    $data = json_decode(file_get_contents(get_template_directory_uri() . "/json/stronger.json"), true);
    foreach ($data as $item) {
        $fields = array(
            'name' => $item['name'],
            'price' => (float)$item['price'],
            'img' => $item['img'],
        );
        $wpdb->insert($wpdb->prefix . 'gl_stronger', $fields, '');
    }

}

//add_action('admin_init', 'fill_gl_gofra_table');

function fill_gl_gofra_table()
{
    global $wpdb;
    $wpdb->query('TRUNCATE ' . $wpdb->prefix . 'gl_gofra');
    $data = json_decode(file_get_contents(get_template_directory_uri() . "/json/gofra.json"), true);
    foreach ($data as $item) {
        $fields = array(
            'name' => $item['name'],
            'diam' => (int)$item['diam'],
            'long' => (int)$item['long'],
            'price' => (float)$item['price'],
            'img' => $item['img'],
        );
        $wpdb->insert($wpdb->prefix . 'gl_gofra', $fields, '');
    }

}

//add_action('admin_init', 'fill_gl_car_ex_table');

function fill_gl_car_ex_table()
{
    global $wpdb;
    $wpdb->query('TRUNCATE ' . $wpdb->prefix . 'gl_attachments');
    $data = json_decode(file_get_contents(get_template_directory_uri() . "/json/CarEx.json"), true);
    foreach ($data as $item) {
        $fields = array(
            'img' => trim($item['img']),
            'name' => trim($item['name']),
            'price' => (float)$item['price'],
            'long' => $item['long'],
            'd_enter' => $item['d_enter'],
            'd_ex' => $item['d_ex'],
        );
        $wpdb->insert($wpdb->prefix . 'gl_attachments', $fields, '');
    }

}

//add_action('admin_init', 'fill_gl_elems_table');

function fill_gl_elems_table()
{
    global $wpdb;
    $wpdb->query('TRUNCATE ' . $wpdb->prefix . 'gl_elems');
    $data = json_decode(file_get_contents(get_template_directory_uri() . "/json/elems.json"), true);
    foreach ($data as $key => $item) {
        if ($key) {
            $fields = array(
                'img' => 'images/catalog/krepeg/' . trim($item['kod']) . '.jpg',
                'name' => trim($item['name']),
                'price' => (float)$item['price'],
                'kod' => $item['kod'],
                'column' => 'elems',
                'dis' => json_encode($item['dis'], JSON_UNESCAPED_UNICODE),
            );
            $wpdb->insert($wpdb->prefix . 'gl_elems', $fields, '');
        }
    }

}

//add_action('admin_init', 'rewrite_names');
function rewrite_names()
{
    global $wpdb;
    $table = $wpdb->prefix . 'gl_stronger';
    $names = $wpdb->get_results('SELECT `id`, `name` FROM `' . $table . '`', ARRAY_A);
    foreach ($names as $name) {
        $column['name'] = str_replace('Труба', '', $name['name']);
        $where['id'] = $name['id'];
        $wpdb->update($table, $column, $where);
    }
}