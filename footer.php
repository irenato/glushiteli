<?php
/**
 * Created by PhpStorm.
 * User: renato
 * Date: 22.04.17
 * Time: 11:59
 */

?>

<footer>
    <div class="container">
        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 ">
                <ul class="footer-inner">
                    <li class="f_item">
                        <p class="title">Меню</p>
                        <ul class="f_menu">
                            <li><a href="<?= get_home_url(); ?>" class="logo"><i class="fa fa-home" aria-hidden="true"></i>Главная</a></li>
                            <?php get_template_part('template-parts/header_menu') ?>
                        </ul>
                    </li>
                    <li class="f_item">
                        <p class="title">Контакты</p>
                        <ul class="cont_list">
                            <?php if (get_option('address')): ?>
                                <li>
                                    <a href="">
                                        <i class="fa fa-map-marker" aria-hidden="true"></i>
                                        <span><?= get_option('address') ?></span>
                                    </a>
                                </li>
                            <?php endif; ?>
                            <?php if (get_option('admin_email')): ?>
                                <li>
                                    <a href="mailto:<?= get_option('admin_email') ?>">
                                        <i class="fa fa-envelope" aria-hidden="true"></i>
                                        <span> <?= get_option('admin_email') ?></span>
                                    </a>
                                </li>
                            <?php endif; ?>
                            <?php if (get_option('phone1')): ?>
                                <li>
                                    <a href="tel:<?= sanitazePhone(get_option('phone1')) ?>">
                                        <i class="fa fa-mobile" aria-hidden="true"></i>
                                        <span><?= get_option('phone1') ?></span>
                                    </a>
                                </li>
                            <?php endif; ?>
                            <?php if (get_option('phone2')): ?>
                                <li>
                                    <a href="tel:<?= sanitazePhone(get_option('phone2')) ?>">
                                        <i class="fa fa-mobile" aria-hidden="true"></i>
                                        <span><?= get_option('phone2') ?></span>
                                    </a>
                                </li>
                            <?php endif; ?>
                            <?php if (get_option('phone3')): ?>
                                <li>
                                    <a href="tel:<?= sanitazePhone(get_option('phone3')) ?>">
                                        <i class="fa fa-mobile" aria-hidden="true"></i>
                                        <span><?= get_option('phone3') ?></span>
                                    </a>
                                </li>
                            <?php endif; ?>
                        </ul>
                    </li>
                    <li class="f_item">
                        <p class="title">Мы в соц.сетях</p>
                        <ul class="social">
                            <?php if (get_option('link_vk')): ?>
                                <li>
                                    <a href="<?= get_option('link_vk') ?>" target="_blank" rel="nofollow">
                                        <i class="fa fa-vk" aria-hidden="true"></i>
                                    </a>
                                </li>
                            <?php endif; ?>
                            <?php if (get_option('link_inst')): ?>
                                <li>
                                    <a href="<?= get_option('link_inst') ?>" target="_blank" rel="nofollow">
                                        <i class="fa fa-instagram" aria-hidden="true"></i>
                                    </a>
                                </li>
                            <?php endif; ?>
                            <?php if (get_option('link_fb')): ?>
                                <li>
                                    <a href="<?= get_option('link_fb') ?>" target="_blank" rel="nofollow">
                                        <i class="fa fa-facebook" aria-hidden="true"></i>
                                    </a>
                                </li>
                            <?php endif; ?>
                            <?php if (get_option('link_odn')): ?>
                                <li>
                                    <a href="<?= get_option('link_odn') ?>" target="_blank" rel="nofollow">
                                        <i class="fa fa-odnoklassniki" aria-hidden="true"></i>
                                    </a>
                                </li>
                            <?php endif; ?>
                            <?php if (get_option('skype')): ?>
                                <li>
                                    <a href="skype: <?= get_option('skype') ?>">
                                        <i class="fa fa-skype" aria-hidden="true"></i>
                                    </a>
                                </li>
                            <?php endif; ?>
                        </ul>
                    </li>
                </ul>
                <p class="p_b"><?= get_option('copyright') ?></p>
                <p class="p_b"><?= get_option('copyright_description') ?></p>
            </div>
        </div>
    </div>

    
</footer>
</body>
<?php wp_footer(); ?>
</html>