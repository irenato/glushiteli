<?php
/**
 * Created by PhpStorm.
 * User: renato
 * Date: 06.05.17
 * Time: 14:11
 */

/**
 * Template name: About us
 */

get_header();

?>

    <!--START CONTENT-->
    <main class="main_p about">
            <div class="banner">
                <div class="banner-outer">
                    <div class="banner-inner">
                        <h1>Страница не найдена!</h1>
                        <a href="<?= get_home_url(); ?>" <h4 class="a_center">на главную</h4></a>
                </div>
            </div>
        <div class="tab_nav">
            <div class="container">
                <div class="row">
                    <?php get_template_part('template-parts/main_menu') ?>
                </div>
            </div>
        </div>
    </main>

<?php

get_footer();
