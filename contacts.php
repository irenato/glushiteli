<?php
/**
 * Created by PhpStorm.
 * User: renato
 * Date: 06.05.17
 * Time: 14:11
 */

/**
 * Template name: Contacts
 */

get_header();

?>

    <!--START CONTENT-->
<?php if (have_posts()) : while (have_posts()) :
    the_post(); ?>
    <main>
        <div class="contacts">
            <div class="container">
                <div class="row">
                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 ">
                        <h3><?php the_title() ?></h3>
                        <p><?php the_content() ?></p>
                        <div class="l_s">
                            <ul class="cont_list">
                                <?php if (get_option('address')): ?>
                                    <li>
                                        <a href="">
                                            <i class="fa fa-map-marker" aria-hidden="true"></i>
                                            <span><?= get_option('address') ?></span>
                                        </a>
                                    </li>
                                <?php endif; ?>
                                <?php if (get_option('admin_email')): ?>
                                    <li>
                                        <a href="mailto:<?= get_option('admin_email') ?>">
                                            <i class="fa fa-envelope" aria-hidden="true"></i>
                                            <span> <?= get_option('admin_email') ?></span>
                                        </a>
                                    </li>
                                <?php endif; ?>
                                <?php if (get_option('phone1')): ?>
                                    <li>
                                        <a href="tel:<?= sanitazePhone(get_option('phone1')) ?>">
                                            <i class="fa fa-mobile" aria-hidden="true"></i>
                                            <span><?= get_option('phone1') ?></span>
                                        </a>
                                    </li>
                                <?php endif; ?>
                                <?php if (get_option('phone2')): ?>
                                    <li>
                                        <a href="tel:<?= sanitazePhone(get_option('phone2')) ?>">
                                            <i class="fa fa-mobile" aria-hidden="true"></i>
                                            <span><?= get_option('phone2') ?></span>
                                        </a>
                                    </li>
                                <?php endif; ?>
                                <?php if (get_option('phone3')): ?>
                                    <li>
                                        <a href="tel:<?= sanitazePhone(get_option('phone3')) ?>">
                                            <i class="fa fa-mobile" aria-hidden="true"></i>
                                            <span><?= get_option('phone3') ?></span>
                                        </a>
                                    </li>
                                <?php endif; ?>
                            </ul>
                            <p class="soc_title">Мы в социальных сетях</p>
                            <ul class="social">
                                <?php if (get_option('link_vk')): ?>
                                    <li>
                                        <a href="<?= get_option('link_vk') ?>" target="_blank" rel="nofollow">
                                            <i class="fa fa-vk" aria-hidden="true"></i>
                                        </a>
                                    </li>
                                <?php endif; ?>
                                <?php if (get_option('link_inst')): ?>
                                    <li>
                                        <a href="<?= get_option('link_inst') ?>" target="_blank" rel="nofollow">
                                            <i class="fa fa-instagram" aria-hidden="true"></i>
                                        </a>
                                    </li>
                                <?php endif; ?>
                                <?php if (get_option('link_fb')): ?>
                                    <li>
                                        <a href="<?= get_option('link_fb') ?>" target="_blank" rel="nofollow">
                                            <i class="fa fa-facebook" aria-hidden="true"></i>
                                        </a>
                                    </li>
                                <?php endif; ?>
                                <?php if (get_option('link_odn')): ?>
                                    <li>
                                        <a href="<?= get_option('link_odn') ?>" target="_blank" rel="nofollow">
                                            <i class="fa fa-odnoklassniki" aria-hidden="true"></i>
                                        </a>
                                    </li>
                                <?php endif; ?>
                                <?php if (get_option('skype')): ?>
                                    <li>
                                        <a href="skype: <?= get_option('skype') ?>">
                                            <i class="fa fa-skype" aria-hidden="true"></i>
                                        </a>
                                    </li>
                                <?php endif; ?>
                            </ul>
                        </div>
                        <div class="r_s">
                            <div id="map"></div>
                        </div>
                        <h4 class="a_center">Оставте свои данные и мы обязательно с свяжемся Вами.</h4>
                        <form action="#" method="post" id="contacts_form" class="a_center">
                            <p class="body-title">Поля помеченные <span>*</span> обязательны для заполнения</p>
                            <div class="form-group">
                                <input type="text" class="form-control" name="firstName" data-valid="firstName"
                                       placeholder="Ваше имя *">
                                <p class="error">Имя должно содержать не менее 3 букв</p>
                            </div>
                            <div class="form-group">
                                <input type="text" class="form-control" name="email" data-valid="email"
                                       placeholder="Ваш e-mail *">
                                <p class="error">Проверте правильность введенного эл.адреса</p>
                            </div>
                            <div class="form-group">
                                <input type="text" class="form-control" name="phone" data-valid="phone"
                                       placeholder="Ваш номер телефона *">
                                <p class="error">Проверте правильность введенного номера телефона</p>
                            </div>
                            <div class="form-group">
                                <input type="text" class="form-control" name="city" placeholder="Ваш город">
                            </div>
                            <div class="form-group">
                                <input type="text" class="form-control" name="year"
                                       placeholder="Год выпуска автомобиля">
                            </div>
                            <div class="form-group">
                                <input type="text" class="form-control" name="piston"
                                       placeholder="Объем двигателя автомобиля">
                            </div>
                            <div class="form-group">
                                <input type="text" class="form-control" name="body"
                                       placeholder="Кузов (седан, хетчбек, универсал)">
                            </div>
                            <div class="form-group">
                                <textarea name="message" class="form-control" placeholder="Сообщение"></textarea>
                            </div>
                            <div class="buttons">
                                <input type="submit" class="button orange" value="Отправить">
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </main>
<?php endwhile; ?>
<?php endif; ?>
<?php

get_footer();
