<?php

add_action('wp_ajax_nopriv_sendMail', 'sendMail');
add_action('wp_ajax_sendMail', 'sendMail');

function sendMail()
{

    $f_arr = explode('&', $_REQUEST['form']);

    // admin`s email
    // get_option('admin_email')
    $recipient = get_option('admin_email');
    $text = '';
    $helpArr = array("Имя", "E-mail", "Номер телефона", "Город", "Год выпуска авто", "Объем двигателя", "Кузов", "Сообщение");

    foreach ($f_arr as $key => $value)
        $f_arr[$key] = explode('=', $value);
    foreach ($f_arr as $key => $arr)
        $text .= $helpArr[$key] . ': ' . urldecode($arr[1]) . '<br>';

    $subject = 'Форма обратной связи!';
    $message = "<div style='font-size: 16px;'><b style='font-size: 20px;'>Форма обратной связи!</b> <br><br>
       Клиент: 
       <br>
       $text </div>";
    $headers = "Content-type: text/html; charset=UTF-8 \r\n";
    //mail() returns true(1) or false(0)
    echo(mail($recipient, $subject, $message, $headers));
    die();


    wp_die();

}


add_action('wp_ajax_nopriv_order', 'order');
add_action('wp_ajax_order', 'order');

function order()
{

    // var_dump($_POST);

    // admin`s email
    // get_option('admin_email')
    $deliveryArea = isset($_POST["deliveryArea"]) ? $_POST["deliveryArea"] : "";
    $deliveryCity = isset($_POST["deliveryCity"]) ? $_POST["deliveryCity"] : "";
    $deliveryWarehouse = isset($_POST["deliveryWarehouse"]) ? $_POST["deliveryWarehouse"] : "";
    $recipient = get_option('admin_email');
    $text = '
        <p>
            <b style="font-size: 14px;"> Имя заказчика: </b>' . $_POST["firstName"] . '
        </p>
        <p>
            <b style="font-size: 14px;"> Еmail: </b>' . $_POST["email"] . '
        </p>
        <p>
            <b style="font-size: 14px;"> Номер телефона: </b>' . $_POST["phone"] . '
        </p>
        <p>
            <b style="font-size: 14px;"> Город: </b>' . $_POST["city"] . '
        </p>
        <p>
            <b style="font-size: 14px;"> Год выпуска авто: </b>' . $_POST["year"] . '
        </p>
        <p>
            <b style="font-size: 14px;"> Объем двигателя: </b> ' . $_POST["piston"] . '
        </p>
        <p>
            <b style="font-size: 14px;"> Кузов: </b> ' . $_POST["body"] . '
        </p>
        <p>
            <b style="font-size: 14px;"> Способ доставки: </b> ' . $_POST["delivery"] . '
        </p>
        <p>
            <b style="font-size: 14px;"> Область доставки (если почтой): </b> 
            ' . $deliveryArea . '
        </p>
        <p>
            <b style="font-size: 14px;"> Город доставки (если почтой): </b> 
            ' . $deliveryCity . '
        </p>
        <p>
            <b style="font-size: 14px;"> Почтовое отделение (если почтой): </b> 
            ' . $deliveryWarehouse . '
        </p>
        <p>
            <b style="font-size: 14px;"> Способ оплаты: </b> ' . $_POST["payment"] . '
        </p>
        <p>
            <b style="font-size: 14px;"> Сообщение: </b> ' . $_POST["message"] . '
        </p> 
        <p>
            <b style="font-size: 20px;"> Заказ: </b> 
            <hr>
        </p> 
    ';

    foreach ($_POST['goods'] as $key => $val)
        // $text .= $helpArr[$key] . ': ' . $arr[1] . '<br>';
        $text .= '<p>' . $_POST['goods'][$key]['name'] . ': ' . $_POST['goods'][$key]['singlePrice'] . ' x ' . $_POST['goods'][$key]['quantity'] . ' = ' . $_POST['goods'][$key]['total_price'] . '</p> <hr>';


    $subject = 'Форма заказа!';
    $message = "<div style='font-size: 16px;'><b style='font-size: 32px;'>Форма заказа!</b> <br><br>
       <b style='font-size: 20px;'>Клиент:</b><hr> $text </div>";
    $headers = "Content-type: text/html; charset=UTF-8 \r\n";
    //mail() returns true(1) or false(0)
    echo(mail($recipient, $subject, $message, $headers));
    die();

    wp_die();
}


