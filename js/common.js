'use strict';

jQuery(document).ready(function ($) {
    var url = '/wp-admin/admin-ajax.php';
    setTimeout(function () {
        $('#preloader').fadeOut(500);
        $('main, header, footer').fadeIn(500);
    }, 1000);

    setTimeout(function () {
        if ($(window).scrollTop() >= 40) {
            $('header.main_p_h').addClass('move');
            $('header .header_contacts').slideUp();
        }
    }, 200);

    //-----------------------------------------------------------
    //  Кросбраузерное событие прокрутки колеса мыши (не скролл документа)
    //  addOnWheel       - повесить событие прокрутки колеса мыши
    //  removeOnWheel    - снять прокрутки колеса мыши
    //-----------------------------------------------------------

    var _body = document.getElementsByTagName('body')[0];

    addOnWheel(_body, sliderScroll);

    function addOnWheel(elem, handler) {
        if (elem.addEventListener) {
            if ('onwheel' in document) {
                // IE9+, FF17+
                elem.addEventListener("wheel", handler);
            } else if ('onmousewheel' in document) {
                // устаревший вариант события
                elem.addEventListener("mousewheel", handler);
            } else {
                // 3.5 <= Firefox < 17, более старое событие DOMMouseScroll пропустим
                elem.addEventListener("MozMousePixelScroll", handler);
            }
        } else {
            // IE8-
            _body.attachEvent("onmousewheel", handler);
        }
    }

    function removeOnWheel(elem, handler) {
        if (elem.removeEventListener) {
            if ('onwheel' in document) {
                // IE9+, FF17+
                elem.removeEventListener("wheel", handler);
            } else if ('onmousewheel' in document) {
                // устаревший вариант события
                elem.removeEventListener("mousewheel", handler);
            } else {
                // 3.5 <= Firefox < 17, более старое событие DOMMouseScroll пропустим
                elem.removeEventListener("MozMousePixelScroll", handler);
            }
        } else {
            // IE8-
            _body.attachEvent("onmousewheel", handler);
        }
    }

    function sliderScroll(event) {

        var event = event || window.event;
        var delta = event.wheelDelta || event.deltaY || event.detail;

        // console.log('---------------------')
        // console.log('[log] delta - ' + delta)
        // console.log('---------------------')

        // for Safari
        if (navigator.userAgent.indexOf('Safari') != -1 && navigator.userAgent.indexOf('Chrome') == -1) {
            event.deltaY > 0 ? moveDown() : moveUp();
        } else {

            // for other browsers   

            // console.log('[log] deltaY - ' + event.deltaY)
            // console.log('[log] wheelDeltaY - ' + event.wheelDeltaY)
            // console.log('[log] wheelDelta - ' + event.wheelDelta)

            if (delta > 0 && !(delta % 120) || delta < 0 && delta > -120 && !(delta % 3) || delta == -152) {

                moveUp();
            } else if (delta < 0 && !(delta % 120) || delta > 0 && !(delta % 3) || delta == 152) {

                moveDown();
            }
        }
        if (navigator.userAgent.indexOf('Gecko') != -1 && navigator.userAgent.indexOf('Macintosh') != -1) {
            event.deltaY > 0 ? moveDown() : moveUp();
        }
    }

    function moveDown() {
        if ($(window).scrollTop() >= 40 && !$('header.main_p_h').hasClass('move')) {
            $('header.main_p_h').addClass('move');
            $('header .header_contacts').slideUp();
        } else if ($('header .header_contacts').is(':visible')) {
            $('header .header_contacts').slideUp();
        }
    }

    function moveUp() {
        if ($(window).scrollTop() < 40 && $('header.main_p_h').hasClass('move')) {
            $('header.main_p_h').removeClass('move');
            $('header .header_contacts').slideDown();
        } else if (!$('header .header_contacts').is(':visible')) {
            $('header .header_contacts').slideDown();
        }
    }

    //=========================================
    // accordion 
    //=========================================

    $('body').on('click', '.title', function () {

        var paretntLi = $(this).closest('li'),
            parentUl = paretntLi.closest('ul'),
            allMenu = $('.box .subMenu');


        if ( paretntLi.hasClass('open') ) {

            paretntLi.removeClass('open');
            paretntLi.find('.subMenu').slideUp(300);

        } else {

            parentUl.find('.open').removeClass('open').end().find('.subMenu').slideUp(300);
            paretntLi.addClass('open').find('> .subMenu').slideDown(300);


        }
    });

    if ( $('.accordion').length && $(window).width() <= 768 ) {
        $('#modal-machine .modal-body').append($('.a_center.s_r_holder'));
        $('.accordion .r_s').remove();
    }


    //=========================================
    // accordion show info
    //=========================================
    $('body').on('click', '.b_inner', function () {
        $(this).closest('.body').find('.info').slideToggle();
    });

    var cl = function cl(val) {
        console.log('===================');
        console.log(val);
        console.log('===================');
    };
    //***************************   Start PAGE  (body class="s-p") *********************************


    //-----------------------------------------------------------------------------
    // Клик на "Параметры поиска" выпадашка
    //-----------------------------------------------------------------------------

    $('body').on('click', '.criterion', function () {
        $(this).closest('.sort-box').toggleClass('open');
    });

    //-----------------------------------------------------------------------------
    // Клик по фильтру подтягивает значение в "Параметры поиска"
    //-----------------------------------------------------------------------------
    $('body').on('click', '.search .o_box a', function (e) {
        e.preventDefault();
        $(this).closest('.sort-box').find('.criterion span').html($(this).html());
        $('#gl_category_name').val($(this).attr('href'));
        $(this).closest('.sort-box').removeClass('open');
        if ($(window).width() < 767) {
            $('.res').html('').append($(this).closest('.sort-box').find('.criterion span').clone());
        }
    });

    //-----------------------------------------------------------------------------
    // Зарытие "Параметры поиска", если клик на body
    //-----------------------------------------------------------------------------
    $('body').on('click', function (e) {
        if (!$(e.target).closest('.search .sort-box').length) {
            $('.search .sort-box').removeClass('open');
        }
    });
    $('body').on('click', '#closeModal', function(e) {
        $("#modal-dialog").modal('hide');
    })
    //-----------------------------------------------------------------------------
    // Сабмит формы
    //-----------------------------------------------------------------------------
    // if ($('#seerch').length) {
    //     $('#search').submit(function () {
    //         var searchValue = $('.search input').val(),
    //             searchParam = $('.sort-box .criterion span').html();
    //         alert('поиск ' + searchValue + ' параметры - ' + searchParam);
    //     });
    // }

    if ($('#contacts_form').length) {

        $('#contacts_form').submit(function (e) {

            e.preventDefault();

            $('[data-valid]').blur();
            // console.log($(this).serialize());

            if (!$(this).find('.has-error').length) {

                $.ajax({
                    url: '/wp-admin/admin-ajax.php',
                    data: {
                        action: 'sendMail',
                        form: $(this).serialize()
                    },
                    method: 'POST',
                    // url: '../functions/mailer.php',
                    success: function success(resp) {
                        if (resp == '1') {
                            // alert('Сообщение отправлено!');
                            $("#modal-dialog").find('.modal-body').html('Сообщение отправлено! В скором времени мы с Вами свяжемся.<div class=\"button green hov\"><a href=\"#\" id=\"closeModal\"> Ок </a></div>').end().modal('show');

                            console.log(resp);
                        } else {
                            // alert('Не удалось отправить сообщение. Сервер не отвечает.');
                            $("#modal-dialog").find('.modal-body').html('Не удалось отправить сообщение. Сервер не отвечает.<div class=\"button green hov\"><a href=\"#\" id=\"closeModal\"> Ок </a></div>').end().modal('show');

                        }
                    }
                });
            }
        });
    }
    // --------------------------------
    // burger menu for mobile
    // --------------------------------
    // $('body').on('click', 'a.menu', function (e) {
    //     e.preventDefault();

    //     $(this).toggleClass('open');
    // });

    // --------------------------------
    // fancybox
    // --------------------------------
    setTimeout(function() {
        $("[rel='gl-photo']").fancybox({
            'transitionIn': 'elastic',
            'transitionOut': 'elastic',
            'speedIn': 600,
            'speedOut': 200,
            'overlayShow': false
        })
    }, 1000)
    

    // --------------------------------
    // scroll to
    // --------------------------------
    $("body").on('click', ".scroll_to", function (e) {
        // body...
        e.preventDefault();
        var top = +$('.' + $(this).attr('href')).offset().top - 70;

        $("html,body").animate({
            scrollTop: top
        }, 500);
    });

    //--------------------------------
    // tooltip
    //--------------------------------
    // $('[data-toggle="tooltip"]').tooltip();


    //==========================================================
    // submit form into cart page ( #order_form  )
    //==========================================================
    $('#order_form').submit(function (event) {
        event.preventDefault();

        if ($(this).find('.step3 input:checked').length) {
            $('.step.current .body-title').removeClass('error');
            //===============
            // ajax
            //===============
        } else {
            $('.step.current .body-title').addClass('error');
        }
    });

    //==========================================================
    // steps of create order into cart page ( #order_form  )
    //==========================================================
    $('body').on('click', '#order_form .button:not(.orange)', function (e) {
        e.preventDefault();

        var action = $(this).data('action'),
            currentIndex = $('.step.current').data('index'),
            parentForm = $('#order_form');

        toggleStep(action, currentIndex, parentForm);
    });

    function toggleStep(action, currentIndex, parentForm) {

        // console.log(action);
        // console.log(currentIndex);
        // console.log(parentForm);

        toggleStep[action][currentIndex](parentForm);
    }
    toggleStep.next = {
        '1': function _(parentForm) {
            $("[data-valid]").blur();

            if (!$('.step.current .has-error').length) this['goTo'](2, parentForm);
        },
        '2': function _(parentForm) {

            var checkedInput = $('.step.current input:checked');

            if (checkedInput.length && !checkedInput.closest('.radio').find('.select').length || checkedInput.closest('.radio').find("[data-change]").length > 2) {
                $('.step.current .body-title').removeClass('error');
                this['goTo'](3, parentForm);
            } else {
                $('.step.current .body-title').addClass('error');
            }
        },
        // '3': function(parentForm) {

        //     if ( parentForm.find('.step3 input:checked').length ) {
        //         $('.step.current .body-title').removeClass('error');
        //         cl('send')
        //         //===============
        //         // ajax
        //         //===============
        //     } else {
        //         $('.step.current .body-title').addClass('error');
        //         cl('error')
        //     }

        // },
        'goTo': function goTo(i, parentForm) {
            parentForm.find('.step').slideUp(500).removeClass('current').end().find('.step' + i).slideDown(500).addClass('current');
        }
    };
    toggleStep.prev = {
        '2': function _(parentForm) {
            toggleStep.next['goTo'](1, parentForm);
        },
        '3': function _(parentForm) {
            toggleStep.next['goTo'](2, parentForm);
        }
    };

    //==========================================================
    // validation of input create order into cart page ( #order_form  )
    //==========================================================
    $("[data-valid]").on('blur', function () {
        validObj.validator(this, $(this).val(), $(this).data('valid'));
    });

    var validObj = {
        validator: function validator(_this, value, key) {

            if (this[key].test(value)) {
                $(_this).closest('.form-group').removeClass('has-error').addClass('has-success');
            } else {
                $(_this).closest('.form-group').addClass('has-error').removeClass('has-success');
            }
        },
        firstName: /[а-яА-Яa-zA-Z]{3,}/,
        email: /.+@.+\..+(\..+)?/,
        phone: /\+38\([0-9]+\)\s[0-9]+-[0-9]+-[0-9]+/
    };

    // --------------------------------------
    //  chanded select and input in #modal_form
    // --------------------------------------
    $('body').on('change', '#modal_form select, #modal_form input', function () {
        $(this).attr('data-change', true);
    });

    jQuery('body').on('change', "[name='email']", function (event) {
        jQuery(this).val(jQuery(this).val().replace(/\s/gi, ''));
    });
    $('#modal_form').submit(function(e) {
        e.preventDefault();

        var payment = $(this).find("[name='optradio1']:checked");

        if ( !payment.length ) {
            return false;
        }

        var checkedRadio = $(this).find("[name='optradio']:checked"),
            selectsHolder = checkedRadio.closest('.radio').find('.select'),
            data = {
                action: "order",
                firstName: $(this).find("[name='firstName']").val(),
                email: $(this).find("[name='email']").val(),
                phone: $(this).find("[name='phone']").val(),
                city: $(this).find("[name='city']").val(),
                year: $(this).find("[name='year']").val(),
                piston: $(this).find("[name='piston']").val(),
                body: $(this).find("[name='body']").val(),
                delivery: checkedRadio.next().find('b').text(),
                payment: payment.next().text(),
                message: $(this).find("textarea").val()
            };

            if ( selectsHolder.length ) {
                data.deliveryArea = selectsHolder.find('.np_area').find('option:selected').text();
                data.deliveryCity = selectsHolder.find('.np_city').find('option:selected').text();
                data.deliveryWarehouse = selectsHolder.find('.np_warehouse').length 
                    ? selectsHolder.find('.np_warehouse').find('option:selected').text() 
                    : selectsHolder.find('input.form-control').val();
            }

            data.goods = [];
            $('.cart_body li').each(function(i,el) {
                data.goods.push({
                    name: $(el).find('.title').text(),
                    singlePrice: $(el).find('.price').text(),
                    quantity: $(el).find('.quantity input').val(),
                    total_price: $(el).find('.total_price').text()
                })
            });


        console.log(data);

        $.ajax({
                    url: '/wp-admin/admin-ajax.php',
                    data: data,
                    method: 'POST',
                    // url: '../functions/mailer.php',
                    success: function success(resp) {
                                                    console.log(resp);

                        if (resp == '1') {
                            // alert('Сообщение отправлено!');
                            // $("#modal-dialog").find('.modal-body').html('Сообщение отправлено! В скором времени мы с Вами свяжемся.<div class=\"button green hov\"><a href=\"#\" id=\"closeModal\"> Ок </a></div>').end().modal('show');

                            console.log(resp);
                        } else {
                            // alert('Не удалось отправить сообщение. Сервер не отвечает.');
                            // $("#modal-dialog").find('.modal-body').html('Не удалось отправить сообщение. Сервер не отвечает.<div class=\"button green hov\"><a href=\"#\" id=\"closeModal\"> Ок </a></div>').end().modal('show');

                        }
                    }
                });







        $('#order_form').slideUp(300);
        $.ajax({
            url: url,
            type: 'POST',
            data: {
                action: 'clearCart',
            },
            success: function(){
                hideTable();
                $('.cart_body li').remove();
            }
        })

        $('html,body').animate({
            scrollTop: 100
        }, 500);
        // $("#modal-dialog").find('.modal-body').html('Спасибо! Ваш заказ принят в обработку. В скором времени мы с Вами свяжемся.<div class=\"button green hov\"><a href=\"#\" id=\"exitFromCart\"> Ок </a></div>').end().modal('show');

    });
    $('body').on('click', '#exitFromCart', function() {
        window.location.href = window.location.origin;
    });
    // --------------------------------------
    //  phome mask
    // --------------------------------------
    $("[data-valid='phone']").focus(function () {
        var str = '+38?(000) 000-00-00';
        $(this).mask(str);
    });

    $("[data-valid='phone']").mask('+38?(000) 000-00-00');

    //--------------------------------------
    // map
    //--------------------------------------
    if (jQuery('#map').length) {
        var init = function () {
            var contactLatlng = {
                lat: 50.025464,
                lng: 36.375684

            };
            var styleMap = [{
                "featureType": "all",
                "elementType": "all",
                "stylers": [{
                    "visibility": "on"
                }]
            }, {
                "featureType": "all",
                "elementType": "geometry",
                "stylers": [{
                    "color": "#f7a50b"
                }, {
                    "visibility": "on"
                }]
            }, {
                "featureType": "all",
                "elementType": "geometry.fill",
                "stylers": [{
                    "visibility": "on"
                }, {
                    "color": "#f7a50b"
                }]
            }, {
                "featureType": "administrative",
                "elementType": "labels.text.fill",
                "stylers": [{
                    "color": "#444444"
                }]
            }, {
                "featureType": "administrative.country",
                "elementType": "geometry.fill",
                "stylers": [{
                    "color": "#ff0000"
                }]
            }, {
                "featureType": "administrative.country",
                "elementType": "geometry.stroke",
                "stylers": [{
                    "color": "#ffffff"
                }]
            }, {
                "featureType": "administrative.province",
                "elementType": "geometry.stroke",
                "stylers": [{
                    "visibility": "off"
                }]
            }, {
                "featureType": "administrative.province",
                "elementType": "labels.text",
                "stylers": [{
                    "visibility": "off"
                }]
            }, {
                "featureType": "administrative.locality",
                "elementType": "geometry.stroke",
                "stylers": [{
                    "visibility": "off"
                }]
            }, {
                "featureType": "administrative.locality",
                "elementType": "labels.text",
                "stylers": [{
                    "visibility": "off"
                }]
            }, {
                "featureType": "administrative.locality",
                "elementType": "labels.text.stroke",
                "stylers": [{
                    "visibility": "off"
                }]
            }, {
                "featureType": "administrative.neighborhood",
                "elementType": "labels",
                "stylers": [{
                    "visibility": "off"
                }]
            }, {
                "featureType": "landscape",
                "elementType": "all",
                "stylers": [{
                    "visibility": "on"
                }]
            }, {
                "featureType": "landscape.man_made",
                "elementType": "geometry.fill",
                "stylers": [{
                    "color": "#f7a50b"
                }, {
                    "visibility": "on"
                }]
            }, {
                "featureType": "landscape.man_made",
                "elementType": "geometry.stroke",
                "stylers": [{
                    "visibility": "off"
                }]
            }, {
                "featureType": "landscape.man_made",
                "elementType": "labels.text",
                "stylers": [{
                    "visibility": "off"
                }]
            }, {
                "featureType": "landscape.man_made",
                "elementType": "labels.text.fill",
                "stylers": [{
                    "color": "#ff0000"
                }, {
                    "visibility": "off"
                }]
            }, {
                "featureType": "landscape.man_made",
                "elementType": "labels.icon",
                "stylers": [{
                    "visibility": "off"
                }]
            }, {
                "featureType": "landscape.natural",
                "elementType": "geometry.fill",
                "stylers": [{
                    "color": "#da8900"
                }]
            }, {
                "featureType": "landscape.natural",
                "elementType": "labels.text",
                "stylers": [{
                    "visibility": "off"
                }]
            }, {
                "featureType": "poi",
                "elementType": "all",
                "stylers": [{
                    "visibility": "off"
                }]
            }, {
                "featureType": "poi",
                "elementType": "geometry",
                "stylers": [{
                    "visibility": "off"
                }]
            }, {
                "featureType": "poi",
                "elementType": "labels",
                "stylers": [{
                    "visibility": "off"
                }]
            }, {
                "featureType": "poi",
                "elementType": "labels.text",
                "stylers": [{
                    "visibility": "off"
                }]
            }, {
                "featureType": "road",
                "elementType": "all",
                "stylers": [{
                    "saturation": -100
                }, {
                    "lightness": 45
                }]
            }, {
                "featureType": "road",
                "elementType": "geometry.fill",
                "stylers": [{
                    "color": "#ff8a00"
                }]
            }, {
                "featureType": "road",
                "elementType": "geometry.stroke",
                "stylers": [{
                    "visibility": "off"
                }]
            }, {
                "featureType": "road",
                "elementType": "labels",
                "stylers": [{
                    "visibility": "off"
                }]
            }, {
                "featureType": "road",
                "elementType": "labels.text",
                "stylers": [{
                    "visibility": "off"
                }]
            }, {
                "featureType": "road",
                "elementType": "labels.text.fill",
                "stylers": [{
                    "visibility": "off"
                }]
            }, {
                "featureType": "road",
                "elementType": "labels.text.stroke",
                "stylers": [{
                    "visibility": "off"
                }]
            }, {
                "featureType": "road",
                "elementType": "labels.icon",
                "stylers": [{
                    "visibility": "off"
                }]
            }, {
                "featureType": "road.highway",
                "elementType": "all",
                "stylers": [{
                    "visibility": "simplified"
                }]
            }, {
                "featureType": "road.highway",
                "elementType": "geometry.fill",
                "stylers": [{
                    "visibility": "on"
                }]
            }, {
                "featureType": "road.highway",
                "elementType": "geometry.stroke",
                "stylers": [{
                    "visibility": "off"
                }]
            }, {
                "featureType": "road.highway",
                "elementType": "labels.text",
                "stylers": [{
                    "visibility": "off"
                }]
            }, {
                "featureType": "road.highway",
                "elementType": "labels.icon",
                "stylers": [{
                    "visibility": "on"
                }]
            }, {
                "featureType": "road.arterial",
                "elementType": "labels.icon",
                "stylers": [{
                    "visibility": "off"
                }]
            }, {
                "featureType": "road.local",
                "elementType": "geometry.stroke",
                "stylers": [{
                    "visibility": "off"
                }]
            }, {
                "featureType": "transit",
                "elementType": "all",
                "stylers": [{
                    "visibility": "off"
                }]
            }, {
                "featureType": "transit",
                "elementType": "labels.icon",
                "stylers": [{
                    "visibility": "off"
                }]
            }, {
                "featureType": "water",
                "elementType": "all",
                "stylers": [{
                    "color": "#f2ba40"
                }, {
                    "visibility": "on"
                }]
            }, {
                "featureType": "water",
                "elementType": "geometry.fill",
                "stylers": [{
                    "color": "#ffffff"
                }]
            }, {
                "featureType": "water",
                "elementType": "labels.text",
                "stylers": [{
                    "visibility": "off"
                }]
            }, {
                "featureType": "water",
                "elementType": "labels.icon",
                "stylers": [{
                    "visibility": "off"
                }]
            }];
            var zoomMap;
            if ($(window).width() > 1200) {
                zoomMap = 17;
            } else {
                zoomMap = 15;
            }
            var contactOptions = {
                zoom: zoomMap,
                styles: styleMap,
                scrollwheel: true,
                draggable: true,
                // center: {
                //     lat: 50.025940,
                //     lng: 36.374010
                // }
                center: {
                    lat: 50.027209,
                    lng: 36.371063
                }
            };
            var x;
            var y;
            if ($(window).width() > 1200) {
                x = 150;
                y = 100;
            } else if ($(window).width() <= 1200 && $(window).width() > 500) {
                x = -50;
                y = 100;
            } else {
                x = -50;
                y = 20;
            }
            
                var contact = document.getElementById('map');
                var contact_map = new google.maps.Map(contact, contactOptions);
                var markerContact = new google.maps.Marker({
                    position: contactLatlng,
                    map: contact_map,
                    title: 'Глушители',
                    icon: window.location.origin + '/wp-content/themes/glushiteli/' + 'images/markerMap.png'
                });
                contact_map.panBy(x, y);

                setTimeout(function() {google.maps.event.trigger(map, 'resize');},1000)
            
        };

        google.maps.event.addDomListener(window, 'load', init);
    }


    //=====================================
    // count of goods into cart page
    //=====================================
    var interval;
    var thisInput,
        timeout,
        thisInputValue,
        bool = true;

    $(".quantity i.fa-plus").on({
        mousedown: function mousedown(event) {
            event.preventDefault();
            thisInput = $(this).closest(".quantity").find("input");
            if (parseInt(thisInput.val()) < 120) {
                thisInput.val(parseInt(thisInput.val()) + 1); // Add 1
                timeout = window.setTimeout(function () {
                    interval = window.setInterval(function () {
                        thisInput.val(parseInt(thisInput.val()) < 120 ? parseInt(thisInput.val()) + 1 : 120);
                    }, 200);
                }, 400);
            } else {
                thisInput.val(120);
            }
            if (bool) {

                bool = false;
            }
        },
        mouseup: function mouseup(event) {
            event.preventDefault();
            window.clearTimeout(timeout);
            window.clearInterval(interval);

            cangeTotalPriceOfOneProduct(thisInput);
        },
        mouseleave: function mouseleave(event) {
            event.preventDefault();
            window.clearTimeout(timeout);
            window.clearInterval(interval);
            cangeTotalPriceOfOneProduct($(this).closest(".quantity").find("input"));
        }
    });

    $(".quantity i.fa-minus").on({
        mousedown: function mousedown(event) {
            event.preventDefault();
            thisInput = $(this).closest(".quantity").find("input");

            if (parseInt(thisInput.val()) > 1) {
                thisInput.val(parseInt(thisInput.val()) - 1);
                timeout = window.setTimeout(function () {
                    interval = window.setInterval(function () {
                        thisInput.val(parseInt(thisInput.val()) > 1 ? parseInt(thisInput.val()) - 1 : 1);
                    }, 200);
                }, 400);
            } else {
                thisInput.val(1);
            }
        },
        mouseup: function mouseup(event) {
            event.preventDefault();
            window.clearTimeout(timeout);
            window.clearInterval(interval);

            cangeTotalPriceOfOneProduct(thisInput);

            bool = true;
        },
        mouseleave: function mouseleave(event) {
            event.preventDefault();
            window.clearTimeout(timeout);
            window.clearInterval(interval);

            cangeTotalPriceOfOneProduct($(this).closest(".quantity").find("input"));
        }
    });

    $(".quantity input").on({
        blur: function blur() {
            thisInputValue = parseInt($(this).val());
            if (thisInputValue > 120) {
                $(this).val(120);
            } else if (thisInputValue < 1) {
                $(this).val(1);
            }
            cangeTotalPriceOfOneProduct($(this));
        },
        keypress: function keypress(event) {

            event = event || window.event;

            if (event.keyCode == 13) {
                thisInputValue = parseInt($(this).val());
                if (thisInputValue > 120) {
                    $(this).val(120);
                } else if (thisInputValue < 1) {
                    $(this).val(1);
                }
                cangeTotalPriceOfOneProduct($(this));
            }

            if (event.charCode && (event.charCode < 48 || event.charCode > 57)) {
                // проверка на event.charCode - чтобы пользователь мог нажать backspace, enter, стрелочку назад...
                return false;
            }
        },
        keyup: function keyup() {
            bool = true;
        }
    });

    //=====================================
    // delete goods into cart page
    //=====================================
    $('body').on('click', '.cart .del', function(e) {
        var _this = $(this);
        $.ajax({
            url: url,
            type: 'POST',
            data: {
                action: 'delFromCart',
                data: $(_this).data('key'),
            },
            success: function(){
                delGoods(_this);
            }
        })
        e.preventDefault();

    });
    function delGoods(_this) {

        $(_this).closest('li').remove();

        if (!$('.cart_body li').length) {
            hideTable();
        }
        cangeTotalPrice();
    }
    function hideTable() {
        $('.cart_wrapp').hide();
        $('#order_form').hide();
        $("[href='#delGoodsInCart']").closest('.button').remove();
        $('.mess').show();
        $('b.cart_count').remove();

        return false;
    }
    //=====================================
    // change total price of one product into cart page
    //=====================================    
    function cangeTotalPriceOfOneProduct(_input) {

        var parent = $(_input).closest('li'),
            oldPrice = parseFloat(parent.find('.price').text()),
            multiplier = +$(_input).val();

        parent.find('.total_price').html((oldPrice * multiplier).toFixed(2) + ' грн.');

        cangeTotalPrice();
    }

    //=====================================
    // change total price 
    //=====================================  
    function cangeTotalPrice() {

        var multiplier = 0,
            price = 0;

        $('.cart_body li').each(function (i, el) {

            multiplier += +$(el).find('input').val();
            price += parseFloat($(el).find('.total_price').text());
        });

        $('.cart_result').find('.r_quantity').html(multiplier).end().find('.r_total_price').html(price.toFixed(2) + ' грн.');
    }
    cangeTotalPrice();
    //=====================================
    // форма заказа
    //===================================== 
    $("[href='#order_form']").click(function (e) {
        e.preventDefault();

        var elemId = $(this).attr('href');

        $(elemId).slideDown(300);

        setTimeout(function () {
            $('html,body').animate({
                scrollTop: $(elemId).offset().top - 100
            }, 500);
        }, 300);
    });
    //=====================================
    // очистить корзину
    //===================================== 
    $("[href='#delGoodsInCart']").click(function (e) {
        e.preventDefault();
        $.ajax({
            url: url,
            type: 'POST',
            data: {
                action: 'clearCart',
            },
            success: function(){
                hideTable();
                $('.cart_body li').remove();                
            }
        })


    });
    //=====================================
    // Новая почта
    //=====================================
    $(document).on('change', 'select.np_area, select.np_city', function(){
        var _this = $(this);
        $.ajax({
            url: url,
            type: 'POST',
            data: {
                action: 'showDataNP',
                ref: $(_this).val(),
                type: $(_this).attr('name'),
            },
            success: function(response){
                response = JSON.parse(response);
                console.log('select.' + response.type);
                var select = $(_this).closest('div.select').find('select.' + response.type);
                $(select).find('option:not(:disabled)').detach();
                if(response.type == "np_city")
                    $(_this).closest('div.select').find('select.np_warehouse').find('option:not(:disabled)').detach();
                $(response.np).each(function (i, item) {
                    var option = document.createElement('option');
                    $(select).append($(option).val(item.ref).text(item.description));
                })
            }
        })
    });
    //================================================
    // открытие навигации при адаптивном режиме
    //================================================
    $('body').on('click', '.tab_nav', function(e) {
        if ( $(window).width() <= 767 )
            $('.tab_nav').toggleClass('hover');
    });
    
});


