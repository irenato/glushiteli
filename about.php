<?php
/**
 * Created by PhpStorm.
 * User: renato
 * Date: 06.05.17
 * Time: 14:11
 */

/**
 * Template name: About us
 */

get_header();

?>

    <!--START CONTENT-->
    <main class="main_p about">
        <?php if (have_posts()) : while (have_posts()) :
            the_post(); ?>
            <div class="banner">
                <div class="banner-outer">
                    <div class="banner-inner">
                        <h1><?php the_title() ?></h1>
                        <h4 class="a_center"><?php the_content(); ?></h4>
                        <div class="search-result">
                            <form action="#" id="search">
                                <div class="search">
                                    <input type="text" placeholder="Поиск...">
                                </div>
                                <div class="res"></div>
                                <div class="buttons">
                                    <input type="submit" class="button orange" value="Поиск">
                                    <div class="button green"><a href="#">Показать все</a></div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        <?php endwhile; ?>
        <?php endif; ?>
        <div class="tab_nav">
            <div class="container">
                <div class="row">
                    <?php get_template_part('template-parts/main_menu') ?>
                </div>
            </div>
        </div>
        <div class="main">
            <div class="container">
                <div class="row">
                    <h4><?= get_field('description'); ?></h4>
                </div>
            </div>
        </div>
    </main>

<?php

get_footer();
