<?php
/**
 * Created by PhpStorm.
 * User: renato
 * Date: 22.04.17
 * Time: 13:00
 */

function glushiteli_styles()
{
    wp_enqueue_style('glushiteli-fonts', get_template_directory_uri() . '/css/fonts.css', '', '', 'all');
    wp_enqueue_style('glushiteli-bootstrap', get_template_directory_uri() . '/css/bootstrap.min.css', '', '', 'all');
    wp_enqueue_style('glushiteli-font-awesome', get_template_directory_uri() . '/css/font-awesome.min.css', '', '', 'all');
    // wp_enqueue_style('glushiteli-slick', get_template_directory_uri() . '/css/slick.css', '', '', 'all');
    // wp_enqueue_style('glushiteli-slick-theme', get_template_directory_uri() . '/css/slick-theme.css', '', '', 'all');
    wp_enqueue_style('glushiteli-jquery.fancybox', get_template_directory_uri() . '/css/jquery.fancybox.css', '', '', 'all');
    wp_enqueue_style('glushiteli-main', get_template_directory_uri() . '/css/main.css', '', '', 'all');
}
add_action('wp_enqueue_scripts', 'glushiteli_styles');

function glushiteli_scripts()
{
        wp_enqueue_script('glushiteli-vue', get_template_directory_uri() . '/js/vue.js', false, '', true);
        wp_enqueue_script('glushiteli-vendor', get_template_directory_uri() . '/js/vendor.js', false, '', true);
        
        
        wp_enqueue_script('glushiteli-gmail', 'https://maps.googleapis.com/maps/api/js?key=AIzaSyBVB_ZUHr5lgoPZIUV-bpPuM5qXd6JfuZ4', false, '', true);
        wp_enqueue_script('glushiteli-common', get_template_directory_uri() . '/js/common.js', false, '', true);
        wp_enqueue_script('glushiteli-action', get_template_directory_uri() . '/js/action.js', false, '', true);
        wp_localize_script('glushiteli-action', 'alevel_ajax', array(
            'ajax_url' => admin_url('admin-ajax.php')
        ));
}

add_action('wp_enqueue_scripts', 'glushiteli_scripts');