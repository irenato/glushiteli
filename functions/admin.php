<?php
/**
 * Created by PhpStorm.
 * User: Khalimov Renato
 * Date: 30.04.2017
 * Time: 11:38
 */

function theme_settings_page()
{

    echo '<div class="wrap">';
    echo '<h1>Настройки магазина</h1>';
    echo '<form method="post" action="options.php" enctype="multipart/form-data">';
    settings_fields("section");
    do_settings_sections("theme-options");
    submit_button();
    echo '</form>';
    echo '</div>';
}

function display_phone1_element()
{
    echo '<input type="text" name="phone1" id="phone1" value="' . get_option('phone1') . '"/>';
}

function display_phone2_element()
{
    echo '<input type="text" name="phone2" id="phone2" value="' . get_option('phone2') . '"/>';
}

function display_phone3_element()
{
    echo '<input type="text" name="phone3" id="phone3" value="' . get_option('phone3') . '"/>';
}

function display_address_element()
{
    echo '<textarea name="address" id="address" cols="45" rows="3">' . get_option('address') . '</textarea>';
}

function display_np_key_element()
{
    echo '<input type="text" name="np_key" id="np_key" value="' . get_option('np_key') . '"/>';
}

function display_link_vk_element()
{
    echo '<input type="text" name="link_vk" id="link_vk" value="' . get_option('link_vk') . '"/>';
}

function display_link_inst_element()
{
    echo '<input type="text" name="link_inst" id="link_inst" value="' . get_option('link_inst') . '"/>';
}

function display_link_fb_element()
{
    echo '<input type="text" name="link_fb" id="link_fb" value="' . get_option('link_fb') . '"/>';
}

function display_link_odn_element()
{
    echo '<input type="text" name="link_odn" id="link_odn" value="' . get_option('link_odn') . '"/>';
}

function display_skype_element()
{
    echo '<input type="text" name="skype" id="skype" value="' . get_option('skype') . '"/>';
}

function display_currency_element()
{
    echo '<input type="text" name="currency" id="currency" value="' . get_option('currency') . '" placeholder="Разделитель - точка"/>';
}

function display_percent_element_polmosrow()
{
    echo '<input type="text" name="percent_polmostrow" id="percent_polmostrow" value="' . get_option('percent_polmostrow') . '" placeholder="Разделитель - точка"/>';
}

function display_percent_element_universal()
{
    echo '<input type="text" name="percent_universal" id="percent_universal" value="' . get_option('percent_universal') . '" placeholder="Разделитель - точка"/>';
}

function display_percent_element_sports()
{
    echo '<input type="text" name="percent_sports" id="percent_sports" value="' . get_option('percent_sports') . '" placeholder="Разделитель - точка"/>';
}

function display_percent_element_gofra()
{
    echo '<input type="text" name="percent_gofra" id="percent_gofra" value="' . get_option('percent_gofra') . '" placeholder="Разделитель - точка"/>';
}

function display_percent_element_stronger()
{
    echo '<input type="text" name="percent_stronger" id="percent_stronger" value="' . get_option('percent_stronger') . '" placeholder="Разделитель - точка"/>';
}

function display_percent_element_tube()
{
    echo '<input type="text" name="percent_tube" id="percent_tube" value="' . get_option('percent_tube') . '" placeholder="Разделитель - точка"/>';
}

function display_percent_element_attachments()
{
    echo '<input type="text" name="percent_attachments" id="percent_attachments" value="' . get_option('percent_attachments') . '" placeholder="Разделитель - точка"/>';
}

function display_percent_element_elems()
{
    echo '<input type="text" name="percent_elems" id="percent_elems" value="' . get_option('percent_elems') . '" placeholder="Разделитель - точка"/>';
}

function display_copyright_element()
{
    echo '<textarea name="copyright" id="copyright" cols="45" rows="3">' . get_option('copyright') . '</textarea>';
}

function display_copyright_description_element()
{
    echo '<textarea name="copyright_description" id="copyright_description" cols="45" rows="3">' . get_option('copyright_description') . '</textarea>';
}


function price_display()
{
    echo '<input type="file" name="price_main"
            accept="application/vnd.openxmlformats-officedocument.spreadsheetml.sheet, application/vnd.ms-excel"/>';
    echo get_option('price_main');
}

function price_universal_display()
{
    echo '<input type="file" name="price_universal" 
            accept="application/vnd.openxmlformats-officedocument.spreadsheetml.sheet, application/vnd.ms-excel"/>';
    echo get_option('price_universal');
}

function price_sports_display()
{
    echo '<input type="file" name="price_sports" 
            accept="application/vnd.openxmlformats-officedocument.spreadsheetml.sheet, application/vnd.ms-excel"/>';
    echo get_option('price_sports');
}

function price_gofra_display()
{
    echo '<input type="file" name="price_gofra" 
            accept="application/vnd.openxmlformats-officedocument.spreadsheetml.sheet, application/vnd.ms-excel"/>';
    echo get_option('price_gofra');
}

function price_stronger_display()
{
    echo '<input type="file" name="price_stronger" 
            accept="application/vnd.openxmlformats-officedocument.spreadsheetml.sheet, application/vnd.ms-excel"/>';
    echo get_option('price_stronger');
}

function price_tube_display()
{
    echo '<input type="file" name="price_tube" id="price_tube"
            accept="application/vnd.openxmlformats-officedocument.spreadsheetml.sheet, application/vnd.ms-excel"/>';
    echo get_option('price_tube');
}

function price_attachments_display()
{
    echo '<input type="file" name="price_attachments" 
            accept="application/vnd.openxmlformats-officedocument.spreadsheetml.sheet, application/vnd.ms-excel"/>';
    echo get_option('price_attachments');
}

function price_elems_display()
{
    echo '<input type="file" name="price_elems" 
            accept="application/vnd.openxmlformats-officedocument.spreadsheetml.sheet, application/vnd.ms-excel"/>';
    echo get_option('price_elems');
}