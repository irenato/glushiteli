<?php
/**
 * Created by PhpStorm.
 * User: Khalimov Renato
 * Date: 23.04.2017
 * Time: 13:28
 */

//add_action( 'init', 'custom_terms' );
function custom_terms() {
    register_taxonomy(
        'news_category',
        array('news'),

        array(
            'labels' => array(
                'name' => 'News Category',
                'add_new_item' => 'Add New Category',
                'new_item_name' => "New Category"
            ),
            'rewrite' => array( 'slug' => 'blog' ), // меняем слаг 'news_category' на слаг 'blog'
            'show_ui' => true,
            'show_tagcloud' => false,
            'hierarchical' => true
        )
    );
}

//add_action('init', 'fill_application_table');

function fill_application_table()
{
    global $wpdb;
    $wpdb->query('TRUNCATE ' . $wpdb->prefix . 'polmo_app');
    $handle = fopen(get_template_directory_uri() . "/polmo_aplikacje_export.csv", "r");
    while (($data = fgetcsv($handle, 0, ",")) !== FALSE) {
        $fields = array(
            'id' => $data[0],
            'plik' => $data[1],
            'manufacturer' => $data[2],
            'model' => $data[3],
            'diesel' => $data[4],
            'cat' => $data[5],
            'carcase' => $data[6],
            'year_from' => $data[7],
            'month_from' => $data[8],
            'year_to' => $data[9],
            'month_to' => $data[10],
            'ccm' => $data[11],
            'hp' => $data[12],
            'kW' => $data[13],
            'euro' => $data[14],
            'description' => $data[15],
            'comment' => $data[16],
            'date' => $data[17],
            'image' => $data[18],
            'version' => $data[19]
        );
        $wpdb->insert($wpdb->prefix . 'polmo_app', $fields, '');
    }

}

//add_action('init', 'fill_manufacturer_table');

function fill_manufacturer_table()
{
    global $wpdb;
    $handle = fopen(get_template_directory_uri() . "/csv/polmo_grupy_export.csv", "r");
    while (($data = fgetcsv($handle, 0, ",")) !== FALSE) {
        $fields = array(
            'id' => $data[0],
            'title' => $data[1]);
        $wpdb->insert($wpdb->prefix . 'polmo_manufacturers', $fields, '');
    }

}

//add_action('init', 'fill_price_table');

function fill_price_table()
{
    global $wpdb;
    $wpdb->query('TRUNCATE ' . $wpdb->prefix . 'polmo_price');
    $handle = fopen(get_template_directory_uri() . "/polmo_polmocennik_export.csv", "r");
    $i = -1;
    while (($data = fgetcsv($handle, 0, ",")) !== FALSE) {
        ++$i;
        if($i != 0) {
            $fields = array(
                'manufacturer_id' => $data[0],
                'polmo_id' => $data[1],
                'polmo_code' => $data[2],
                'title_pl' => $data[3],
                'title_en' => $data[4],
                'description' => $data[5],
                'zam' => $data[6],
                'oe' => $data[7],
                'status' => $data[8],
                'weight' => $data[9],
                'price' => $data[11],
                'data_c' => $data[12],
                'data_z' => $data[13],
                'attention' => $data[15],
                'image' => $data[17],
            );
            $wpdb->insert($wpdb->prefix . 'polmo_price', $fields, '');
        }
    }

}

//add_action('init', 'fill_group_table');

function fill_group_table()
{
    global $wpdb;
    $handle = fopen(get_template_directory_uri() . "/polmo_polmogrupy_export.csv", "r");
    while (($data = fgetcsv($handle, 0, ",")) !== FALSE) {
        $fields = array(
            'polmo_code' => $data[0],
            'group_id' => $data[1]);
        $wpdb->insert($wpdb->prefix . 'polmo_groups', $fields, '');
    }

}

//add_action('init', 'fill_brands_table');

function fill_brands_table()
{
    global $wpdb;
    $wpdb->query('TRUNCATE ' . $wpdb->prefix . 'polmo_brands');
    $i = -1;
    $handle = fopen(get_template_directory_uri() . "/polmo_polmokros_export.csv", "r");
    while (($data = fgetcsv($handle, 0, ",")) !== FALSE) {
        ++$i;
        if($i != 0) {
        $fields = array(
            'polmo_code' => $data[0],
            'cross_code' => $data[1],
            'original' => mb_strtolower($data[2]) == 'true' ? 1 : 0,
            'brand' => $data[3],
            'difference' => $data[4],
        );
        $wpdb->insert($wpdb->prefix . 'polmo_brands', $fields, '');
        }
    }

}

//add_action('init', 'fill_s_table');

function fill_s_table()
{
    global $wpdb;
    $wpdb->query('TRUNCATE ' . $wpdb->prefix . 'polmo_s');
    $i = -1;
    $handle = fopen(get_template_directory_uri() . "/polmo_sklady_export.csv", "r");
    while (($data = fgetcsv($handle, 0, ",")) !== FALSE) {
        ++$i;
        if($i != 0) {
            $fields = array(
                'id' => $data[0],
                'polmo_code' => $data[1],
                'oe' => $data[2],
            );
            $wpdb->insert($wpdb->prefix . 'polmo_s', $fields, '');
        }
    }

}