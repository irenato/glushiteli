<?php
/**
 * Created by PhpStorm.
 * User: macbook
 * Date: 15.05.17
 * Time: 22:50
 */

?>

<div class="search-result">
    <form id="search" action="<?= get_permalink(86) ?>" method="post">

        <div class="search">
            <input type="text" name="search" placeholder="Найти <?= mb_strtolower(get_the_title()) ?>" v-model="name"
                   @keyup="searchInArr($event)" v-bind:class="{ 'highlightSearchInput': highlightSearchInput }"
                   @blur="clearHighlight()">
            <p v-if="highlightSearchInput">Ключевое слово должно содержать не менее 3-х букв</p>

            <div class="sort-box">
                <p class="criterion">
                    <i class="icon-arrow_down f_right"></i>
                    <i class="icon-mobile_parameters_1"></i>
                    <span>Параметры поиска</span>
                </p>
                <input id="gl_category_name" name="type" type="hidden" >
                <ul class="o_box">
                    <li><a href="universal">Универсальные глушители</a></li>
                    <li><a href="gofra">Гофры</a></li>
                    <li><a href="attachments">Насадки на глушители</a></li>
                    <li><a href="sports">Спортивные глушители</a></li>
                    <li><a href="stronger">Стронгеры</a></li>
                    <li><a href="tube">Трубы</a></li>
                    <li><a href="elems">Крепежные элементы</a></li>
                    <li><a href="all">По всем разделам</a></li>
                </ul>
            </div>
            <div class="res"></div>
        </div>


        <div class="buttons">
            <div class="button orange"><input type="submit" value="Поиск"></div>
            <div class="button green"><a href="#" v-on:click="showAll($event)">Показать все</a></div>
        </div>
    </form>
</div>