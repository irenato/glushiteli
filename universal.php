<?php
/**
 * Created by PhpStorm.
 * User: renato
 * Date: 02.05.17
 * Time: 20:43
 * глушители-универсальные
 */

/**
 * Template name: Universal
 */

get_header();

?>

<!--START CONTENT-->
<main class="p_universal glyshaki">
    <div class="banner">
        <div class="banner-outer">
            <div class="banner-inner">
                <div class="container">
                    <div class="row">
                        <?php if (have_posts()) : while (have_posts()) :
                            the_post(); ?>
                            <h4 class="a_center"><?php the_title(); ?></h4>
                            <p class="d_text">
                                <?= strip_tags(get_the_content()); ?>

                            </p>
                            <?php if (get_field('description')): ?>
                            <p class="d_text"><?= get_field('description') ?></p>
                        <?php endif; ?>
                            <?php get_template_part('template-parts/search_form') ?>
                        <?php endwhile; ?>
                        <?php endif; ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="tab_nav">
        <div class="container">
            <div class="row">
                <?php get_template_part('template-parts/main_menu') ?>
            </div>
        </div>
    </div>
    <div class="main universal">
        <div class="container">
            <div class="row">

                <div class="a_center pagination" v-if="paginationShow">
                    <a href="#start" v-on:click="changePage($event)"> << </a>
                    <a href="#prev" v-on:click="changePage($event)"> < </a>
                    <span>{{ currentPage }} из {{ allPages }}</span>
                    <a href="#next" v-on:click="changePage($event)"> > </a>
                    <a href="#end" v-on:click="changePage($event)"> >> </a>
                </div>

                <div class="a_center s_r_holder" id="glyshaki-universal">



                    <p id="hiddenData" style="display: none;" data-view="4"><?= showData('universal') ?></p>

                    <p v-if="showMessNotFountResults" class="mess"> По запросу {{ query }} в разделе "<?= mb_strtolower(get_the_title()) ?>" ничего не найдено. 
                    <br> Нажмите кнопку "Поиск" или клавишу enter  для поиска по всему сайту. </p>

                    <div class="s_r_box"
                         v-for="glyshakiUniversal in glyshakiArr | filterBy query in 'full_name' | count | limitBy viewedCards startTo">


                        <h4 class="title"> {{ glyshakiUniversal.full_name }} </h4>

                        <a class="img-holder" rel="gl-photo" href="{{ glyshakiUniversal.img }}"><img
                                src="{{ glyshakiUniversal.img }}"
                                alt=" {{ glyshakiUniversal.full_name}} "></a>
                        <div class="disc-wrapp">
                            <div class="disc">
                                <ul class="info-box">
                                    <li><span class="label">Производитель:</span><span class="text"> {{ glyshakiUniversal.producer }} </span>
                                    </li>
                                    <li><span class="label">Длина:</span><span class="text"> {{ glyshakiUniversal.long }} мм</span>
                                    </li>
                                    <li><span class="label">Диаметр:</span><span class="text"> {{ glyshakiUniversal.width }} мм</span>
                                    </li>
                                    <li><span class="label">Диаметр вход:</span><span class="text"> {{ glyshakiUniversal.d_enter }} мм</span>
                                    </li>
                                    <li><span class="label">Диаметр выход:</span><span class="text"> {{ glyshakiUniversal.d_ex }} мм</span>
                                    </li>
                                    <li><span class="label">Расположение входа:</span><span class="text"> {{ glyshakiUniversal.full_name | enter_position }} </span>
                                    </li>
                                    <li><span class="label">Расположение выхода:</span><span class="text"> {{ glyshakiUniversal.full_name | exit_position }} </span>
                                    </li>
                                    <li><span class="label">Код:</span><span
                                            class="text"> {{ glyshakiUniversal.name}} </span></li>
                                    <li><span class="label">Стоимость доставки <br> (к покупателю):</span><span
                                            class="text">35 - 45 грн</span></li>
                                </ul>
                                <div class="r_s">
                                    <div class="price">
                                        <p class="pr">Стоимость:</p>
                                        <p class="new_pr"> {{ glyshakiUniversal.price }} грн.</p>
                                    </div>
                                    <div class="buttons">
                                        <div class="button green hov"><a href="#" @click="add($event, $index, glyshakiUniversal.id, 'universal')"><i class="fa fa-shopping-bag"
                                                                                     aria-hidden="true"></i>В
                                                корзину</a></div>
                                        <div class="button orange hov"><a href="#" @click="buy($event, $index, glyshakiUniversal.id, 'universal')"><i class="fa fa-shopping-cart"></i>Купить</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>

                <div class="a_center pagination" v-if="paginationShow">
                    <a href="#start" v-on:click="changePage($event)"> << </a>
                    <a href="#prev" v-on:click="changePage($event)"> < </a>
                    <span>{{ currentPage }} из {{ allPages }}</span>
                    <a href="#next" v-on:click="changePage($event)"> > </a>
                    <a href="#end" v-on:click="changePage($event)"> >> </a>
                </div>
            </div>
        </div>
    </div>
</main>


<?php
get_footer();
?>
