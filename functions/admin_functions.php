<?php
/**
 * Created by PhpStorm.
 * User: renato
 * Date: 01.05.17
 * Time: 13:17
 */
/**
 * polmostrow
 */
function handle_price_main_upload()
{
    if (!empty($_FILES["price_main"]["tmp_name"])) {
        global $wpdb;
        $urls = wp_handle_upload($_FILES["price_main"], array('test_form' => FALSE));
        $file_type = PHPExcel_IOFactory::identify($urls["file"]);
        $objReader = PHPExcel_IOFactory::createReader($file_type);
        $objReader->setReadDataOnly(true);
        $objPHPExcel = $objReader->load($urls["file"]);
        $sheetData = $objPHPExcel->getActiveSheet()->toArray(null, true, true, true);
        $test = array();
        foreach ($sheetData as $item) {
            if ($item['F'] != '') {
                $polmo_code = explode(' ', $item['A'])[0];
                $table = $wpdb->prefix . 'polmo_price';
                $field['price'] = (float)$item['F'];
                $where['polmo_code'] = trim($polmo_code);
                $wpdb->update($table, $field, $where);
                array_push($test, $item);
            }
        }

        unlink($urls['file']);
        return 'Данные уcпешно обновлены: ' . date('d-m-Y H:i:s');
    }
    return get_option('price_main');
}

/**
 * @return string
 * universal
 */
function handle_price_universal_upload()
{
    if (!empty($_FILES["price_universal"]["tmp_name"])) {
        global $wpdb;
        $urls = wp_handle_upload($_FILES["price_universal"], array('test_form' => FALSE));
        $file_type = PHPExcel_IOFactory::identify($urls["file"]);
        $objReader = PHPExcel_IOFactory::createReader($file_type);
        $objReader->setReadDataOnly(true);
        $objPHPExcel = $objReader->load($urls["file"]);
        $sheetData = $objPHPExcel->getActiveSheet()->toArray(null, true, true, true);
        foreach ($sheetData as $item) {
            $name = explode(' ', $item['A'])[0];
            $table = $wpdb->prefix . 'gl_universal';
            $field['price'] = $item['F'];
            $where['name'] = $name;
            $wpdb->update($table, $field, $where);
        }
        unlink($urls['file']);
        return 'Данные уcпешно обновлены: ' . date('d-m-Y H:i:s');
    }
    return get_option('price_universal');
}

/**
 * @return string
 * sports
 */
function handle_price_sports_upload()
{
    if (!empty($_FILES["price_sports"]["tmp_name"])) {
        global $wpdb;
        $urls = wp_handle_upload($_FILES["price_sports"], array('test_form' => FALSE));
        $file_type = PHPExcel_IOFactory::identify($urls["file"]);
        $objReader = PHPExcel_IOFactory::createReader($file_type);
        $objReader->setReadDataOnly(true);
        $objPHPExcel = $objReader->load($urls["file"]);
        $sheetData = $objPHPExcel->getActiveSheet()->toArray(null, true, true, true);
        foreach ($sheetData as $item) {
            $code = explode(' ', $item['A'])[0];
            $table = $wpdb->prefix . 'gl_sports';
            $field['price'] = (float)$item['F'];
            $where['code'] = $code;
            $wpdb->update($table, $field, $where);
        }
        unlink($urls['file']);
        return 'Данные уcпешно обновлены: ' . date('d-m-Y H:i:s');
    }
    return get_option('price_sports');
}

/**
 * @return string
 * gofra
 */
function handle_price_gofra_upload()
{
    if (!empty($_FILES["price_gofra"]["tmp_name"])) {
        global $wpdb;
        $urls = wp_handle_upload($_FILES["price_gofra"], array('test_form' => FALSE));
        $file_type = PHPExcel_IOFactory::identify($urls["file"]);
        $objReader = PHPExcel_IOFactory::createReader($file_type);
        $objReader->setReadDataOnly(true);
        $objPHPExcel = $objReader->load($urls["file"]);
        $sheetData = $objPHPExcel->getActiveSheet()->toArray(null, true, true, true);
        foreach ($sheetData as $item) {
            $table = $wpdb->prefix . 'gl_gofra';
            $where['name'] = $item['A'];
            $field['price'] = (float)$item['F'];
            $wpdb->update($table, $field, $where);
        }
        unlink($urls['file']);
        return 'Данные уcпешно обновлены: ' . date('d-m-Y H:i:s');
    }
    return get_option('price_gofra');
}

/**
 * @return string
 * gofra
 */
function handle_price_stronger_upload()
{
    if (!empty($_FILES["price_stronger"]["tmp_name"])) {
        global $wpdb;
        $urls = wp_handle_upload($_FILES["price_stronger"], array('test_form' => FALSE));
        $file_type = PHPExcel_IOFactory::identify($urls["file"]);
        $objReader = PHPExcel_IOFactory::createReader($file_type);
        $objReader->setReadDataOnly(true);
        $objPHPExcel = $objReader->load($urls["file"]);
        $sheetData = $objPHPExcel->getActiveSheet()->toArray(null, true, true, true);
        unset($sheetData[0]);
        foreach ($sheetData as $item) {
            if ($item['F'] != NULL) {
                $item['A'] = explode(' ', $item['A'])[1];
                $wpdb->query("
                        UPDATE `" . $wpdb->prefix . "gl_stronger` 
                        SET price='" . (float)$item['F'] . "' 
                        WHERE name LIKE '%" . $item['A'] . "%'");
            }
        }
        unlink($urls['file']);
        return 'Данные уcпешно обновлены: ' . date('d-m-Y H:i:s');
    }
    return get_option('price_stronger');
}

/**
 * @return string
 * tube
 */
function handle_price_tube_upload()
{
    if (!empty($_FILES["price_tube"]["tmp_name"])) {
        global $wpdb;
        $urls = wp_handle_upload($_FILES["price_tube"], array('test_form' => FALSE));
        $file_type = PHPExcel_IOFactory::identify($urls["file"]);
        $objReader = PHPExcel_IOFactory::createReader($file_type);
        $objReader->setReadDataOnly(true);
        $objPHPExcel = $objReader->load($urls["file"]);
        $sheetData = $objPHPExcel->getActiveSheet()->toArray(null, true, true, true);
        unset($sheetData[1]);
        foreach ($sheetData as $item) {
            $item['A'] = str_replace('труба', '', $item['A']);
            $wpdb->query("
                        UPDATE `" . $wpdb->prefix . "gl_tube` 
                        SET price='" . (float)$item['F'] . "' 
                        WHERE name LIKE '%" . $item['A'] . "'");
        }
        unlink($urls['file']);
        return 'Данные уcпешно обновлены: ' . date('d-m-Y H:i:s');
    }
    return get_option('price_tube');
}

/**
 * @return string
 * tube
 */
function handle_price_attachments_upload()
{
    if (!empty($_FILES["price_attachments"]["tmp_name"])) {
        global $wpdb;
        $urls = wp_handle_upload($_FILES["price_attachments"], array('test_form' => FALSE));
        $file_type = PHPExcel_IOFactory::identify($urls["file"]);
        $objReader = PHPExcel_IOFactory::createReader($file_type);
        $objReader->setReadDataOnly(true);
        $objPHPExcel = $objReader->load($urls["file"]);
        $sheetData = $objPHPExcel->getActiveSheet()->toArray(null, true, true, true);
        foreach ($sheetData as $item) {
            $table = $wpdb->prefix . 'gl_attachments';
            $item['A'] = explode(' ', $item['A'])[0];
            $wpdb->query("
                        UPDATE `" . $wpdb->prefix . "gl_attachments` 
                        SET price='" . (float)$item['F'] . "' 
                        WHERE name LIKE '%" . $item['A'] . "'");
        }
        unlink($urls['file']);
        return 'Данные уcпешно обновлены: ' . date('d-m-Y H:i:s');
    }
    return get_option('price_attachments');
}

/**
 * @return string
 * tube
 */
function handle_price_elems_upload()
{
    if (!empty($_FILES["price_elems"]["tmp_name"])) {
        global $wpdb;
        $urls = wp_handle_upload($_FILES["price_elems"], array('test_form' => FALSE));
        $file_type = PHPExcel_IOFactory::identify($urls["file"]);
        $objReader = PHPExcel_IOFactory::createReader($file_type);
        $objReader->setReadDataOnly(true);
        $objPHPExcel = $objReader->load($urls["file"]);
        $sheetData = $objPHPExcel->getActiveSheet()->toArray(null, true, true, true);
        foreach ($sheetData as $item) {
            $table = $wpdb->prefix . 'gl_elems';
            $item['A'] = explode(' ', $item['A'])[0];
            $wpdb->query("
                        UPDATE `" . $wpdb->prefix . "gl_elems` 
                        SET price='" . (float)$item['F'] . "' 
                        WHERE kod LIKE '%" . $item['A'] . "'");
        }
        unlink($urls['file']);
        return 'Данные уcпешно обновлены: ' . date('d-m-Y H:i:s');
    }
    return get_option('price_elems');
}

/**
 *
 */
function display_theme_panel_fields()
{
    add_settings_section("section", "", null, "theme-options");
    add_settings_field("price_main", "Прайс основной", "price_display", "theme-options", "section");
    add_settings_field("price_universal", "Прайс (универсальные глушители)", "price_universal_display", "theme-options", "section");
    add_settings_field("price_sports", "Прайс (спортивные глушители)", "price_sports_display", "theme-options", "section");
    add_settings_field("price_gofra", "Прайс (гофра)", "price_gofra_display", "theme-options", "section");
    add_settings_field("price_stronger", "Прайс (стронгеры)", "price_stronger_display", "theme-options", "section");
    add_settings_field("price_tube", "Прайс (трубы)", "price_tube_display", "theme-options", "section");
    add_settings_field("price_attachments", "Прайс (насадки)", "price_attachments_display", "theme-options", "section");
    add_settings_field("price_elems", "Прайс (крепеж)", "price_elems_display", "theme-options", "section");
    add_settings_field("currency", "Курс евро", "display_currency_element", "theme-options", "section");
    add_settings_field("percent_polmostrow", "Процент (polmosrow)", "display_percent_element_polmosrow", "theme-options", "section");
    add_settings_field("percent_universal", "Процент (универсальные)", "display_percent_element_universal", "theme-options", "section");
    add_settings_field("percent_sports", "Процент (спортивные)", "display_percent_element_sports", "theme-options", "section");
    add_settings_field("percent_gofra", "Процент (гофра)", "display_percent_element_gofra", "theme-options", "section");
    add_settings_field("percent_stronger", "Процент (стронгер)", "display_percent_element_stronger", "theme-options", "section");
    add_settings_field("percent_tube", "Процент (трубы)", "display_percent_element_tube", "theme-options", "section");
    add_settings_field("percent_attachments", "Процент (насадки)", "display_percent_element_attachments", "theme-options", "section");
    add_settings_field("percent_elems", "Процент (крепеж)", "display_percent_element_elems", "theme-options", "section");
    add_settings_field("phone1", "Контактный номер телефона", "display_phone1_element", "theme-options", "section");
    add_settings_field("phone2", "Контактный номер телефона", "display_phone2_element", "theme-options", "section");
    add_settings_field("phone3", "Контактный номер телефона", "display_phone3_element", "theme-options", "section");
    add_settings_field("skype", "Skype", "display_skype_element", "theme-options", "section");
    add_settings_field("address", "Адрес", "display_address_element", "theme-options", "section");
    add_settings_field("link_vk", "Профиль Вконтакте", "display_link_vk_element", "theme-options", "section");
    add_settings_field("link_inst", "Профиль Instagram", "display_link_inst_element", "theme-options", "section");
    add_settings_field("link_fb", "Профиль Facebook", "display_link_fb_element", "theme-options", "section");
    add_settings_field("link_odn", "Профиль Одноклассники", "display_link_odn_element", "theme-options", "section");
    add_settings_field("copyright", "Copyright", "display_copyright_element", "theme-options", "section");
    add_settings_field("copyright_description", "Copyright (описание)", "display_copyright_description_element", "theme-options", "section");
    add_settings_field("np_key", "Ключ 'НОВАЯ ПОЧТА'", "display_np_key_element", "theme-options", "section");

    register_setting("section", "price_main", "handle_price_main_upload");
    register_setting("section", "price_universal", "handle_price_universal_upload");
    register_setting("section", "price_sports", "handle_price_sports_upload");
    register_setting("section", "price_gofra", "handle_price_gofra_upload");
    register_setting("section", "price_stronger", "handle_price_stronger_upload");
    register_setting("section", "price_tube", "handle_price_tube_upload");
    register_setting("section", "price_attachments", "handle_price_attachments_upload");
    register_setting("section", "price_elems", "handle_price_elems_upload");
    register_setting("section", "currency");
    register_setting("section", "percent_polmostrow");
    register_setting("section", "percent_universal");
    register_setting("section", "percent_sports");
    register_setting("section", "percent_gofra");
    register_setting("section", "percent_stronger");
    register_setting("section", "percent_tube");
    register_setting("section", "percent_attachments");
    register_setting("section", "percent_elems");
    register_setting("section", "phone1");
    register_setting("section", "phone2");
    register_setting("section", "phone3");
    register_setting("section", "skype");
    register_setting("section", "address");
    register_setting("section", "link_vk");
    register_setting("section", "link_inst");
    register_setting("section", "link_fb");
    register_setting("section", "link_odn");
    register_setting("section", "copyright");
    register_setting("section", "copyright_description");
    register_setting("section", "np_key");
}

add_action("admin_init", "display_theme_panel_fields");

function add_theme_menu_item()
{
    add_menu_page("Настройки магазина", "Настройки магазина", "manage_options", "theme-panel", "theme_settings_page", null, 99);
}

add_action("admin_menu", "add_theme_menu_item");