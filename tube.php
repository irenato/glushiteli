<?php
/**
 * Created by PhpStorm.
 * User: renato
 * Date: 02.05.17
 * Time: 20:43
 * глушители-универсальные
 */

/**
 * Template name: Tube
 */

get_header();

?>

<!--START CONTENT-->
<main class="p_nozzles p_stroger glyshaki">
    <div class="banner">
        <div class="banner-outer">
            <div class="banner-inner">
                <div class="container">
                    <div class="row">
                        <!-- <h4 class="a_center">Трубы</h4> -->
                        <?php if (have_posts()) : while (have_posts()) :
                            the_post(); ?>
                            <h4 class="a_center"><?php the_title(); ?></h4>
                            <p class="d_text">
                                <?= strip_tags(get_the_content()); ?>

                            </p>
                                <?php if (get_field('description')): ?>
                                    <p class="d_text"><?= get_field('description') ?></p>
                                <?php endif; ?>
                            <?php endwhile; ?>
                        <?php endif; ?>
                        <?php get_template_part('template-parts/search_form') ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="tab_nav">
        <div class="container">
            <div class="row">
                <?php get_template_part('template-parts/main_menu') ?>
            </div>
        </div>
    </div>
    <div class="main nozzles truba">
        <div class="container">
            <div class="row">

                <div class="a_center pagination" v-if="paginationShow">
                    <a href="#start" v-on:click="changePage($event)"> << </a>
                    <a href="#prev" v-on:click="changePage($event)"> < </a>
                    <span>{{ currentPage }} из {{ allPages }}</span>
                    <a href="#next" v-on:click="changePage($event)"> > </a>
                    <a href="#end" v-on:click="changePage($event)"> >> </a>
                </div>

                <div class="a_center s_r_holder" id="glyshaki-truba">

                    <p id="hiddenData" style="display: none;" data-view="6"><?= showData('tube') ?></p>
                    <p v-if="showMessNotFountResults" class="mess"> По запросу {{ query }} в разделе "<?= mb_strtolower(get_the_title()) ?>" ничего не найдено. 
                        <br> Нажмите кнопку "Поиск" или клавишу enter  для поиска по всему сайту. </p>

                    <div class="s_r_box" v-for="glyshak in glyshakiArr | filterBy query in 'name' | count | limitBy viewedCards startTo">
                        <h4 class="title"> Труба  {{ glyshak.name }}  (цена за 1 метр)</h4>
                        <a class="img-holder" rel="gl-photo" href="{{ 'images/catalog/truba/truba.jpg' | fullSrc }}"><img src="{{ 'images/catalog/truba/truba.jpg' | fullSrc }}" alt=" {{ glyshak.name }} "></a>
                        <div class="disc-wrapp">
                            <div class="disc">
                                <ul class="info-box">
                                    <li><span class="label">Диаметр:</span><span class="text"> {{ glyshak.name | getDiamFromTrubaName }} мм</span></li>
                                    <li><span class="label">Толщина:</span><span class="text"> {{ glyshak.name | getWidthFromTrubaName }} мм</span></li>
                                    
                                    <li><span class="label">Стоимость доставки <br> (к покупателю):</span><span class="text">35 - 45 грн</span></li>
                                </ul>
                                <div class="r_s">
                                    <div class="price">
                                        <p class="pr">Стоимость:</p>
                                        <p class="new_pr"> {{ glyshak.price }} грн.</p>
                                    </div>
                                    <div class="buttons">

                                        <div class="button green hov"><a href="#" @click="add($event, $index, glyshak.id, 'tube')">
                                            <i class="fa fa-shopping-bag" aria-hidden="true"></i>Вкорзину</a>
                                        </div>

                                        <div class="button orange hov"><a href="#" @click="buy($event, $index, glyshak.id, 'tube')">
                                            <i class="fa fa-shopping-cart"></i>Купить</a>
                                        </div>
                                        
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>

                <div class="a_center pagination" v-if="paginationShow">
                    <a href="#start" v-on:click="changePage($event)"> << </a>
                    <a href="#prev" v-on:click="changePage($event)"> < </a>
                    <span>{{ currentPage }} из {{ allPages }}</span>
                    <a href="#next" v-on:click="changePage($event)"> > </a>
                    <a href="#end" v-on:click="changePage($event)"> >> </a>
                </div>
            </div>
        </div>
    </div>
</main>


<?php
get_footer();
?>
