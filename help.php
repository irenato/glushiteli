<?php
/**
 * Created by PhpStorm.
 * User: renato
 * Date: 06.05.17
 * Time: 14:11
 */

/**
 * Template name: Help
 */

get_header();

?>

    <!--START CONTENT-->
    <main class="main_p about">
        <?php if (have_posts()) : while (have_posts()) :
            the_post(); ?>
            <div class="banner">
                <div class="banner-outer">
                    <div class="banner-inner">
                        <div class="container">
                            <div class="row">
                                <h4 class="a_center"><?php get_the_content() ?></h4>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="tab_nav">
                <div class="container">
                    <div class="row">
                        <?php get_template_part('template-parts/main_menu') ?>
                    </div>
                </div>
            </div>
            <div class="main help">
                <div class="container">
                    <div class="row">
                        <?php $help_block_items = get_field('help_block'); ?>
                        <?php if (is_array($help_block_items)): ?>
                            <?php foreach ($help_block_items as $item): ?>
                                <h3><?= $item['question'] ?></h3>
                                <p class="text"><?= $item['answer'] ?></p>
                            <?php endforeach; ?>
                        <?php endif; ?>
                        <div class="contacts">
                            <h4 class="a_center">Если Вы не нашли информацию, которая Вас интересовала oставте свои
                                данные и мы
                                обязательно с свяжемся Вами.</h4>
                            <form action="#" method="post" id="contacts_form" class="a_center">
                                <p class="body-title">Поля помеченные <span>*</span> обязательны для заполнения</p>
                                <div class="form-group">
                                    <input type="text" class="form-control" name="firstName" data-valid="firstName"
                                           placeholder="Ваше имя *">
                                    <p class="error">Имя должно содержать не менее 3 букв</p>
                                </div>
                                <div class="form-group">
                                    <input type="text" class="form-control" name="email" data-valid="email"
                                           placeholder="Ваш e-mail *">
                                    <p class="error">Проверте правильность введенного эл.адреса</p>
                                </div>
                                <div class="form-group">
                                    <input type="text" class="form-control" name="phone" data-valid="phone"
                                           placeholder="Ваш номер телефона *">
                                    <p class="error">Проверте правильность введенного номера телефона</p>
                                </div>
                                <div class="form-group">
                                    <input type="text" class="form-control" name="city" placeholder="Ваш город">
                                </div>
                                <div class="form-group">
                                    <input type="text" class="form-control" name="year"
                                           placeholder="Год выпуска автомобиля">
                                </div>
                                <div class="form-group">
                                    <input type="text" class="form-control" name="piston"
                                           placeholder="Объем двигателя автомобиля">
                                </div>
                                <div class="form-group">
                                    <input type="text" class="form-control" name="body"
                                           placeholder="Кузов (седан, хетчбек, универсал)">
                                </div>
                                <div class="form-group">
                                    <textarea name="message" class="form-control" placeholder="Сообщение"></textarea>
                                </div>
                                <div class="buttons">
                                    <input type="submit" class="button orange" value="Отправить">
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        <?php endwhile; ?>
        <?php endif; ?>
    </main>
<?php

get_footer();
