<?php
/**
 * Created by PhpStorm.
 * User: renato
 * Date: 22.04.17
 * Time: 11:59
 */

?>
    <!DOCTYPE html>
    <html lang="ru">

    <head>
        <meta charset="UTF-8">
        <!-- <meta name="viewport" content="width=1200"> -->
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title> <?= wp_get_document_title('|', true, 'right'); ?></title>
        <?php wp_head(); ?>
    </head>

<body id="glyshaki">


<!-- Modal -->
<div id="modal-dialog" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
      </div>
      <div class="modal-body">
        
            {{ modalText }}

      </div>
    </div>
  </div>
</div>
<div id="modal-machine" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
      </div>
      <div class="modal-body">

      </div>
    </div>
  </div>
</div>

<div id="preloader">
    <div class="preloader-wrapp">
        <div class="preloader">

            <h1>Glushiteli.kh.ua</h1>
        
            <div class="preloader white">
                <div class="preloader ogange">
                    <div class="preloader green">
                        <div class="preloader yellow"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
    <!--START HEADER-->
<?php if (is_front_page()): ?>
    <header class="main_p_h">
        <div class="container header_nav">
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <span class="logo"><i class="fa fa-home" aria-hidden="true"></i>Главная</span>
                    <!-- <a href="/" class="logo">lorem imput</a> -->
                    <div class="right-menu">
                        <ul class="mob-hidden nav">
                            <li>
                                <a href="<?= get_home_url(); ?>">Глушители</a>
                                <?php get_template_part('template-parts/header_submenu') ?>
                            </li>

                            <?php get_template_part('template-parts/header_menu') ?>
                        </ul>
                        <a href="<?= get_page_link(60); ?>" class="cart" @click="cheackCart($event)">
                            <i class="fa fa-shopping-cart" aria-hidden="true"></i>
                            <?php if (isset($_SESSION['cart'])): ?>
                                <b class="cart_count"><?= count($_SESSION['cart']) ?></b>
                            <?php endif; ?>
                        </a>
                        <a href="#" class="menu mob-show" v-on:click="showMobMenu = !showMobMenu">
                            <i class="fa fa-bars" aria-hidden="true"></i>
                        </a>
                        <ul class="mob-showUl" v-if="showMobMenu" transition="expand">
                            <li>
                                <!-- <a href="<?= get_home_url(); ?>">Глушители</a> -->
                                <a href="#">Глушители</a>
                                <?php get_template_part('template-parts/header_submenu') ?>
                            </li>
                            <?php get_template_part('template-parts/header_menu') ?>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <div class="header_contacts">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <ul>
                        <?php if (get_option('phone1')): ?>
                            <li>
                                <a href="tel:<?= sanitazePhone(get_option('phone1')) ?>">
                                    <i class="fa fa-mobile" aria-hidden="true"></i>
                                    <span><?= get_option('phone1') ?></span>
                                </a>,
                            </li>
                        <?php endif; ?>
                        <?php if (get_option('phone2')): ?>
                            <li>
                                <a href="tel:<?= sanitazePhone(get_option('phone2')) ?>">
                                    <span><?= get_option('phone2') ?></span>
                                </a>,
                            </li>
                        <?php endif; ?>
                        <?php if (get_option('phone3')): ?>
                            <li>
                                <a href="tel:<?= sanitazePhone(get_option('phone3')) ?>">
                                    <span><?= get_option('phone3') ?></span>
                                </a>
                            </li>
                        <?php endif; ?>
                    </ul>
                </div>
            </div>
        </div>
        </div>
    </header>
<?php else: ?>
    <header>
        <div class="container header_nav">
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <a href="/" class="logo"><i class="fa fa-home" aria-hidden="true"></i>Главная</a>
                    <div class="right-menu">
                        <ul class="mob-hidden nav">
                            <li>
                                <a href="<?= get_home_url() ?>">Глушители</a>
                                <?php get_template_part('template-parts/header_submenu') ?>
                            </li>
                            <?php get_template_part('template-parts/header_menu') ?>
                        </ul>
                        <a href="<?= get_page_link(60); ?>" class="cart" @click="cheackCart($event)">
                            <i class="fa fa-shopping-cart" aria-hidden="true"></i>
                            <?php if (isset($_SESSION['cart'])): ?>
                                <b class="cart_count"><?= count($_SESSION['cart']) ?></b>
                            <?php endif; ?>
                        </a>
                        <a href="#" class="menu mob-show" v-on:click="showMobMenu = !showMobMenu">
                            <i class="fa fa-bars" aria-hidden="true"></i>
                        </a>
                        <ul class="mob-showUl" v-if="showMobMenu" transition="expand">
                            <li>
                                <!-- <a href="<?= get_home_url(); ?>">Глушители</a> -->
                                <a href="#">Глушители</a>
                                <?php get_template_part('template-parts/header_submenu') ?>
                            </li>
                            <?php get_template_part('template-parts/header_menu') ?>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <div class="header_contacts">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">


                        <ul>
                            <?php if (get_option('phone1')): ?>
                                <li>
                                    <a href="tel:<?= sanitazePhone(get_option('phone1')) ?>">
                                        <i class="fa fa-mobile" aria-hidden="true"></i>
                                        <span><?= get_option('phone1') ?></span>
                                    </a>
                                    ,
                                </li>
                            <?php endif; ?>
                            <?php if (get_option('phone2')): ?>
                                <li>
                                    <a href="tel:<?= sanitazePhone(get_option('phone2')) ?>">
                                        <span> <?= get_option('phone2') ?></span>
                                    </a>
                                    ,
                                </li>
                            <?php endif; ?>
                            <?php if (get_option('phone3')): ?>
                                <li>
                                    <a href="tel:<?= sanitazePhone(get_option('phone3')) ?>">
                                        <span><?= get_option('phone3') ?></span>
                                    </a>
                                </li>
                            <?php endif; ?>
                        </ul>

                    </div>
                </div>
            </div>
        </div>
    </header>
<?php endif; ?>

<?php

$page_id = $post->ID;
