<?php
/**
 * Created by PhpStorm.
 * User: renato
 * Date: 22.04.17
 * Time: 13:34
 */

get_header();
?>
    <!--START CONTENT-->
    <main class="main_p" id="app">
        <?php if (have_posts()) : while (have_posts()) :
        the_post(); ?>
        <div class="banner">
            <div class="banner-outer">
                <div class="banner-inner">
                    <h1><?php the_title(); ?></h1>
                    <h4 class="a_center"><?php the_content() ?></h4>
                    <?php get_template_part('template-parts/search_form') ?>
                </div>
            </div>
            <?php endwhile; ?>
            <?php endif; ?>
        </div>
        <div class="tab_nav">
            <div class="container">
                <div class="row">
                    <?php get_template_part('template-parts/main_menu') ?>
                </div>
            </div>
        </div>
        <div class="main accordion">
            <div class="container">
                <div class="row">
                    <div class="l_s">
                        <div class="box">
                            <ul class="mainMenu">
                                <?php $manufacturers = showManufacturers(); ?>
                                <?php if ($manufacturers) ?>
                                <?php foreach ($manufacturers as $k => $v): ?>
                                    <li>
                                    <span class="title">
                                        <i class="fa fa-plus-square show_models" aria-hidden="true"></i>
                                        <?= $k ?>
                                    </span>
                                        <ul class="subMenu">
                                            <?php foreach ($v as $key => $value): ?>

                                                <li><span class="title"><?= $key ?></span>
                                                    <ul class="subMenu">
                                                        <?php foreach ($value as $item => $subitem): ?>
                                                            <li>
                                                                <a href="#" @click="showMeMoreStupidMachine($event)"
                                                                   class="polmo-show-more"
                                                                   data-id="manufacturer=<?= $k ?>&model=<?= $key ?>&version=<?= $subitem ?>">
                                                                    <?= $subitem ?>
                                                                </a>
                                                            </li>
                                                        <?php endforeach; ?>
                                                    </ul>
                                                </li>

                                            <?php endforeach; ?>
                                        </ul>
                                    </li>
                                <?php endforeach; ?>
                            </ul>
                        </div>
                    </div>


                    <!-- Вывод -->
                    <div class="r_s">
                        <div class="a_center s_r_holder" v-if=" switch === 'foundInfo' ">
                            <p class="mess foundInfo">По запросу <span> {{ infoName }} </span> найдено: </p>
                            <div class="header">
                                <div class="b_inner">
                                    <span class="name">Название</span>
                                    <span>Тип</span>
                                    <span>Год выпуска</span>
                                    <!-- <span>kW</span> -->
                                    <span>HP</span>
                                    <!-- <span>Примечания</span> -->
                                </div>
                            </div>
                            <div class="body" v-for="item in infoArr">
                                <div class="b_inner">
                                    <span class="name"> {{ infoName }} </span>
                                    <span> {{ item.carcase }} </span>
                                    <span> {{ item.year_from }} - {{ item.year_to }} </span>
                                    <!-- <span></span> -->
                                    <span> {{ item.hp }} </span>
                                    <!-- <span></span> -->
                                </div>
                                <div class="info">
                                        
                                    <a class="img-holder" rel="gl-photo" href="images/catalog/polmo/img.gif">
                                        <img :src="data:image/gif;base64,{{ item.image }}" alt="some text">
                                    </a>

                                    <div class="price">
                                        <p class="pr">Стоимость:</p>
                                        <p class="new_pr"> {{ item.price }} грн.</p>
                                    </div>

                                    <div class="buttons">
                                        <div class="button green hov"><a href="#" @click="add($event, $index, item.polmo_code, 'polmostrow')"><i class="fa fa-shopping-bag"
                                                                                     aria-hidden="true"></i>В
                                                корзину</a></div>
                                        <div class="button orange hov"><a href="#" @click="buy($event, $index, item.polmo_code, 'polmostrow')"><i class="fa fa-shopping-cart"></i>Купить</a>
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>

                        <div class="a_center s_r_holder" v-if=" switch === 'notFoundInfo' ">
                            <p class="mess">По данному запросу ничего не найдено</p>
                        </div>

                        <div class="a_center s_r_holder" v-if=" switch === 'preloader' ">

                            <div class="preloader">
                                <div class="ogange">
                                    <div class="green">
                                        <div class="yellow"></div>
                                    </div>
                                </div>
                            </div>

                        </div>

                        <div class="a_center s_r_holder" v-if=" switch === false ">
                            <p class="mess">Выберите марку и модель автомобиля</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </main>

<?php
get_footer();
