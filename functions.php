<?php
/**
 * Created by PhpStorm.
 * User: renato
 * Date: 22.04.17
 * Time: 13:15
 */

include('functions/scripts_and_styles.php');
include('functions/posmostrow_functions.php');
include('functions/polmo_ajax.php');
include('libraries/np/NovaPoshtaApi2.php');
include('libraries/np/NovaPoshtaApi2Areas.php');
include('functions/np.php');
include('functions/mailer.php');

if (is_admin()) {
    include('functions/admin.php');
    include('functions/admin_functions.php');
    include 'libraries/PHPExcel.php';
    include 'libraries/PHPExcel/IOFactory.php';
//    include('functions/csv_to_db.php');
//    include 'functions/json_to_db.php';
}

add_theme_support('post-thumbnails');

function themeslug_theme_customizer($wp_customize)
{
    $wp_customize->add_section('themeslug_logo_section', array(
        'title' => __('Logo', 'themeslug'),
        'priority' => 30,
        'description' => 'Upload a logo to replace the default site name and description in the header',
    ));

    $wp_customize->add_setting('themeslug_logo');
    $wp_customize->add_control(new WP_Customize_Image_Control($wp_customize, 'themeslug_logo', array(
        'label' => __('Logo', 'themeslug'),
        'section' => 'themeslug_logo_section',
        'settings' => 'themeslug_logo',
    )));
}

add_action('init', 'omyblog_init_session', 1);
if (!function_exists('omyblog_init_session')):
    function omyblog_init_session()
    {
        session_start();
    }
endif;

