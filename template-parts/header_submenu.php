<?php
/**
 * Created by PhpStorm.
 * User: renato
 * Date: 06.05.17
 * Time: 19:24
 */

wp_nav_menu(array(
    'theme_location' => '',
    'menu' => 2,
    'container' => false,
    'container_class' => '',
    'container_id' => '',
    'menu_class' => 'sub_nav',
    'menu_id' => '',
    'echo' => true,
    'fallback_cb' => 'wp_page_menu',
    'before' => '',
    'after' => '',
    'link_before' => '',
    'link_after' => '',
    'items_wrap' => '<ul id="%1$s" class="%2$s">%3$s</ul>',
    'depth' => 0,
    'walker' => '',
));