<?php
/**
 * Created by PhpStorm.
 * User: renato
 * Date: 22.04.17
 * Time: 11:58
 */

//get_header();

if (is_front_page()) {
    get_template_part('home');
} else if (is_single()) {
    get_template_part('single');
}

//get_footer();
