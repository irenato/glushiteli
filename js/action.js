/**
 * Created by Аркадий on 23.04.2017.
 */
$(document).ready(function() {
    var url = '/wp-admin/admin-ajax.php';

    glyshaki = new Vue({
        el: '#glyshaki',
        created: function() {

           this.glyshakiArr = $('#hiddenData').length ? JSON.parse($('#hiddenData').html()) : [];
           this.glyshakiArrDefault = JSON.parse(JSON.stringify(this.glyshakiArr));
           this.arrLength = this.glyshakiArr.length;
           this.viewedCardsDefault = this.viewedCards = $('#hiddenData').length ? +$('#hiddenData').data('view') : 0;
           // $('#hiddenData').remove();

           var params = decodeURIComponent(window.location.hash);

           ( params && params.length ) ? ( params.indexOf('s_p') === 1 ? 
                ( 
                    ( +params.substr(4) > 0 && +params.substr(4) <= this.allPages ) ?
                    ( this.currentPage = +params.substr(4), 
                    this.startTo = (this.currentPage - 1) *  this.viewedCards, 
                    this.name = this.query = '') : ( this.currentPage = 1, this.startTo = 0, this.name = this.query = '', window.location.hash= '' )
                    ) 
            :
                ( params.indexOf('%') == -1 ) && (
                    this.currentPage = 1, 
                    this.startTo = 0, 
                    this.name = params.substr(1), 
                    this.showMatch(params.substr(1)) )) 
           : (this.currentPage = 1, this.startTo = 0, this.name = this.query = '') 

        },
        data: {
            infoArr: [],
            infoName: '',
            switch: false,
            showMobMenu: false,
            startTo: 0,
            currentPage: 1,
            query: '',
            paginationShow: true,
            highlightSearchInput: false,
            modalText: 'Товар добавлен в корзину',
            changePageActionObj: {
                start: function() {
                    if ( glyshaki.currentPage !== 1 ) {
                        glyshaki.currentPage = 1;
                        glyshaki.startTo = 0;
                        glyshaki.setPageInURL(1);
                        glyshaki.upDateFancyBox();
                    }
                },
                end: function() {
                    if ( glyshaki.currentPage !== glyshaki.allPages ) {
                        glyshaki.currentPage = glyshaki.allPages;
                        glyshaki.startTo = (glyshaki.currentPage - 1) * glyshaki.viewedCards;
                        glyshaki.setPageInURL(glyshaki.currentPage);
                        glyshaki.upDateFancyBox();
                    }
                },
                next: function() {
                    if ( glyshaki.currentPage !== glyshaki.allPages ) {
                        glyshaki.startTo = glyshaki.currentPage * glyshaki.viewedCards;
                        glyshaki.currentPage++;
                        glyshaki.setPageInURL(glyshaki.currentPage);
                        glyshaki.upDateFancyBox();
                    }
                },
                prev: function() {
                    if ( glyshaki.currentPage !== 1 ) {
                        glyshaki.currentPage--;
                        glyshaki.startTo = (glyshaki.currentPage - 1) * glyshaki.viewedCards;
                        glyshaki.setPageInURL(glyshaki.currentPage);
                        glyshaki.upDateFancyBox();
                    }
                }
            }
        },
        computed: {
            allPages: function () {
                return Math.round((this.arrLength - 1) / this.viewedCards);
            }
        },
        methods: {
            upDateFancyBox: function() {
                setTimeout(function() {
                    $("[rel='gl-photo']").fancybox({
                        'transitionIn': 'elastic',
                        'transitionOut': 'elastic',
                        'speedIn': 600,
                        'speedOut': 200,
                        'overlayShow': false
                    })
                }, 500)
            },
            setSearchInHash: function(searchStr) {
                window.location.hash = encodeURIComponent(searchStr);
            },
            setPageInURL: function(page) {
                window.location.hash = '#s_p' + page;
            },
            showMeMoreStupidMachine: function(e) {
                e.preventDefault();

                var _this = e.target,
                    _that = this;

                this.switch = 'preloader';

                this.infoName =  $(_this).closest('.subMenu').closest('li').closest('.subMenu').prev().text() + ' ' + $(_this).closest('.subMenu').prev().text() + ' ' + $(_this).text();
                
                $('.body .info').slideUp();

                $.ajax({
                    url: url,
                    type: 'POST',
                    data: {
                        'action': 'showMeMoreStupidMachine',
                        'data': $(_this).data('id'),
                    },
                    success: function(result) {

                        !$('header').hasClass('move') && $('header').addClass('move');

                        $('html, body').animate({
                            scrollTop: 200
                        }, 500);

                        console.log(result)
                        _that.infoArr = JSON.parse(result);
                        

                        if ( _that.infoArr.length ) {
                             _that.switch = 'foundInfo';
                        } else {
                            _that.switch = 'notFoundInfo';
                        }

                        if ( $(window).width() <= 768 ) {
                            $('#modal-machine').modal('show');
                        }
                    },
                });
            },
            toArray: function(str) {
                if ( !str ) return [];
                return JSON.parse(str);
            },
            changePage: function($event) {
                $event.preventDefault();

                var t = $event.target,
                    action = t.href.split('#')[1];
                    this.changePageActionObj[action]();
            },
            scrollTo: function($event, target) {
                

                if ( target ) {

                    var to = target.offset().top - 100;

                    $('body, html').animate({
                        scrollTop: to
                    }, 500);

                    return false;
                }
                if ( this.highlightSearchInput ) return false;

                $event.preventDefault();

                if ( this.name.length > 2 ) this.searchInAllTables();

            },
            showAll: function($event) {
                $event.preventDefault();

                this.setSearchInHash('');
                this.query = this.name = '';
                this.showMessNotFountResults = !1;
                this.startTo = 1;               // не сметь удалять эту запись!!! это гениальный костыль -.емое. Жаль, что я бросил пить - а чтобы было?)))
                this.viewedCards = 999999999;   // можно и больше, а че уж там? :)
                this.paginationShow = !1;
                this.startTo = 0;
                this.scrollTo(!1, $('.a_center.s_r_holder'));
            },
            searchInArr: function($event) {

                // if ( !this.name || !this.name.length) return false;

                if ($event.keyCode === 13 && this.name.length > 2 ) {
                    this.searchInAllTables();
                    return false;
                }

                this.showMatch(this.name);


            },
            searchInAllTables: function() {
                var criterion,
                    dataSearch = $('.sort-box .criterion span').attr('data-search');

                // выбранный критерий поиска если есть или "все", если ничего не выбрали 
                dataSearch ? criterion = dataSearch : criterion = 'all';

                // console.log(this.name, dataSearch)
                //
                // $.ajax({
                //     url: url,
                //     type: 'POST',
                //     data: {
                //         'action': 'searchResult',
                //         'data': {
                //             search: this.name,
                //             type: dataSearch,
                //         },
                //     },
                //     success: function(res){
                //         console.log(res);
                //     }
                // });
            },
            clearHighlight: function() {

                if ( !this.query.length ) {

                    this.highlightSearchInput = true;
                    window.search.querySelector('input').value = this.name;

                } else if ( this.query.length < 2) {
                    this.highlightSearchInput = true;
                    window.search.querySelector('input').value = this.name;

                } else {

                    this.highlightSearchInput = false;
                    window.search.querySelector('input').value = this.query;
                }
            },
            showMatch: function(str) {
                str.length > 2 ? ( 
                    this.startTo = 1, 
                    this.highlightSearchInput = false, 
                    this.query = str, 
                    this.paginationShow = false, 
                    this.viewedCards = 999999999, 
                    this.startTo = 0,
                    this.setSearchInHash(str),
                    this.upDateFancyBox() ) 
                : ( this.highlightSearchInput = true,
                    this.glyshakiArr = this.glyshakiArrDefault,
                    this.query = '',
                    this.viewedCards = this.viewedCardsDefault,
                    this.paginationShow = true,
                    this.startTo = 0,
                    this.currentPage = 1,
                    this.$set('showMessNotFountResults', !1),
                    this.setSearchInHash(''),
                    this.upDateFancyBox() );
            },
            cheackCart: function(e) {

                if ( !$('b.cart_count').length ) {
                    this.modalText = 'В Вашей корзине пока нет товаров. Воспользуйтесь быстрой навигацией для выбора категории и поиска нужного товара';
                    $("#modal-dialog").modal('show');
                    e.preventDefault();
                }

            },
            add: function(e, i, id, str) {
                e.preventDefault();
                $.ajax({
                    url: url,
                    type: 'POST',
                    data: {
                        action: 'addToCart',
                        data: {
                            'id': id,
                            'type': str,
                        },
                    },
                    success: function(result){
                        if( $('b.cart_count').length > 0 )
                            $('b.cart_count').text(result);
                        else{
                            var b = document.createElement('b');
                            $('a.cart').append($(b).addClass('cart_count').text(result));
                        }
                        glyshaki.modalText = 'Товар добавлен в корзину';
                        $("#modal-dialog").modal('show');
                    }
                })
            },
            buy: function(e, i, id, str) {
                e.preventDefault();
                $.ajax({
                    url: url,
                    type: 'POST',
                    data: {
                        action: 'addToCart',
                        data: {
                            'id': id,
                            'type': str,
                        },
                    },
                    success: function(result){
                        if( $('b.cart_count').length > 0 )
                            $('b.cart_count').text(result);
                        else{
                            var b = document.createElement('b');
                            $('a.cart').append($(b).addClass('cart_count').text(result));
                        }
                        window.location.href = $('a.cart').attr('href');
                    }
                })
                // console.log('%c' + id, 'color: green; font-size: 14px;')
                // console.log('%c' + str, 'color: green; font-size: 14px;')
            },
            getNewImage: function(name, isBig) {

                if ( name.indexOf('гофра') < 0 ) {
                    return false;
                }
                
                if ( isBig && name.indexOf('W') > -1 ) {
                    return window.location.origin + '/wp-content/themes/glushiteli/images/catalog/gofra/gofraW.jpg';
                } else if ( isBig ) {
                    return window.location.origin + '/wp-content/themes/glushiteli/images/catalog/gofra/gofra.jpg';
                } else {
                    return window.location.origin + '/wp-content/themes/glushiteli/images/catalog/gofra/mainGofra.jpg';
                }
            },
            strongerInSearch: function(str) {

                if ( str.indexOf('Стронгер') > -1 ) {
                    return true;
                } else {
                    return false;
                }

            }
        },
        filters: {
            enter_position: function (str) {
                if ( !str ) return false;
                // str = str.split(' ')[2].split('/')[0] === 'ц' ? 'Сбоку' : 'Центральное';
                str = str.split(' ')[2].split('/')[0];
                if ( str == "ц" ) {
                    return 'Центральное';
                } else {
                    return 'Сбоку';
                }
                // return str;
            },
            exit_position: function (str) {
                if ( !str ) return false;
                str = str.split(' ')[2].split('/')[1] === 'ц' ? 'Сбоку' : 'Центральное';
                return str;
            },
            stronger_long: function (str) {
                return str.split(' ')[1].split('/')[1];
            },
            stronger_diam: function (str) {
                return str.split(' ')[1].split('/')[0];
            },
            count: function (arr) {
                // record length
                if ( !arr.length ) {
                    this.$set('showMessNotFountResults', !0);
                } else {
                    this.$set('showMessNotFountResults', !1);
                }
                return arr
            
            },
            cleanUnnecessary: function (arr) {
                var a = [];
                arr.forEach(function(el, i) {
                    if ( el[0] && el[0].length && el[0].indexOf(' доставки') < 0 ) {
                        a.push(el);
                    }
                })
                 
                return a;
            
            },
            getDiamFromTrubaName: function (str) {
                return str.split(' ')[0];
            },
            getWidthFromTrubaName: function (str) {
                return str.split(' ')[2];
            },
            fullSrc: function (value) {
              return window.location.origin + '/wp-content/themes/glushiteli/' + value;
            }
        }
    });
    //==============================================================
    // FILTERS
    //==============================================================
    // Vue.filter('fullSrc', function (value) {
    //   return window.location.origin + '/wp-content/themes/glushiteli/' + value;
    // });

    // Vue.filter('replaceMistakes', function (value) {
    //   return value.replace('glushiteliimages', 'glushiteli/images');
    // });

    // Vue.filter('some', function (str) {
    //   return str.split(' ')[2].split('/')[0] == 'б' ? 'Сбоку' : 'Центральное';
    // });

    // Vue.filter('IssueYearFilter', function (value) {

    //     if ( value.match(/[0-9]{2}-[0-9]{2}/) ||  value.match(/[0-9]{2}-/)) {
    //         return value.match(regIssueYearFilter)[0]
    //     }
    
    // });
})
